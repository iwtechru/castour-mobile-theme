<?php

get_header();
global $theme;
$dog_num = types_render_field("dogovor_num", array());
if ($current_user->allcaps['manage_options'] || $current_user->user_login == $dog_num || $current_user->has_cap('editor')) {
    get_template_part('dogovor', 'content');
} else {
    echo 'У Вас нет доступа к этой странице.';
}
get_footer();
