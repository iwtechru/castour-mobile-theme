<?php
global $theme;
$items = ($theme->menu_items('Верхнее меню (iwt-mob)'));
if (1 == 1) {
    ?>
    <nav class="navbar  iwt1-navbar" role="navigation">
        <div class="container-fluid">
            <div class="row">
                <div class="navbar-header navbar-header-bordered">
                    <a class="iwt1-logo-mobile" href="<?php echo get_site_url();?>">
                        <img src="<?php bloginfo('template_url'); ?>/css/img/logo2.png" alt="" class="img-responsive"/>
                    </a>
                    <button type="button" class="navbar-toggle iwt1-toggle-btn pull-right" 
                            data-toggle="collapse" data-target="#show-menu">
                        <span class="iwt1-toggle-icon"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="show-menu">
                    <?php echo iwt_draw_mobmenu($items); ?>
                </div>
            </div>
        </div>
    </nav>
<?php } else { ?>
    <ul class="iwt1-darkmenu-mobile">
        <li class="iwt1-li-mobile iwt1-li-mobile-first clearfix">
            <a href="#" class="iwt1-li-mobile-bluelink">Проверить  готовность</a>
            <button type="button" class="btn iwt1-orange-btn iwt1-orange-btn-mobile" 
                    data-toggle="modal" data-target="#callme">Перезвоните мне</button>
        </li>
        <li class="iwt1-li-mobile">
            <div class="clearfix iwt1-darkmenu-title">
                <span class="pull-left">Услуги</span>
                <span class="iwt1-arrow pull-right"></span>
            </div>
            <ul class="iwt1-submenu-mobile">
                <li>
                    <a href="#">УСЛУГИ!!!!!!!!!!!!</a>
                </li>
                <li>
                    <a href="#">Загранпаспорт нового образца</a>
                </li>
                <li>
                    <a href="#">Загранпаспорт старого образца</a>
                </li>
                <li>
                    <a href="#">Помощь в заполнении анкеты</a>
                </li>
                <li>
                    <a href="#">Загранпаспорт для крымчан</a>
                </li>
                <li>
                    <a href="#">Срочное оформление</a>
                </li>
            </ul>
        </li>
        <li class="iwt1-li-mobile">
            <div class="clearfix iwt1-darkmenu-title">
                <span class="pull-left">Полезное</span>
                <span class="iwt1-arrow pull-right"></span>
            </div>

        </li>
        <li class="iwt1-li-mobile">

            <div class="clearfix iwt1-darkmenu-title">
                <span class="pull-left">Справочная</span>
                <span class="iwt1-arrow pull-right"></span>
            </div>
        </li>
        <li class="iwt1-li-mobile">

            <div class="clearfix iwt1-darkmenu-title">
                <span class="pull-left">О компании</span>
                <span class="iwt1-arrow pull-right"></span>
            </div>
        </li>
        <li class="iwt1-li-mobile">
            <div class="clearfix iwt1-darkmenu-title">
                <span class="pull-left">Контакты</span>
                <span class="iwt1-arrow pull-right"></span>
            </div>

        </li>
    </ul><?php } ?>