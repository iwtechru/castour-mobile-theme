<?php
require('includes/functions_class.php');
$theme = new rht_theme('iwtech theme');
$theme->delete_option('required_plugins');
//$theme->disable_comments();

foreach ($fonts as $font) {
    // $theme->update_option('styles', $font, $font);
}
$theme->update_option('menu_defaults', 'container', false);
$theme->update_option('menu_defaults', 'container_class', false);
$theme->update_option('menu_defaults', 'menu_class', 'nav nav-pills');

add_action('pre_get_posts', 'clear_codes');

function clear_codes() {
    remove_all_shortcodes();
    add_shortcode('su_spoiler', 'iwt_su_spoiler');
    //remove_shortcode('su-spoiler');
    //remove_all
}

function iwt_su_spoiler($content, $tag) {
    $out.='<li><a href="#" class="iwt1-faq-list-sublink">';
    $out.=$content['title'];
    $out.='</a>';
    $out.='<div class = "iwt1-faq-block">';
    $out.=$tag;
    $out.='</div>';
    $out.='</li>';
    return $out;
}

function iwt_draw_mobmenu($items,$parent=0){
    $i=0;
    $out='';
    foreach($items as $item){        
        if($item->menu_item_parent == $parent){
         if($parent==0){     
             $out.='<div class="iwt1-li-mobile">';        
             $out.='<a ';
             if($item->menu_children){
                 $out.=' role="button" data-toggle="collapse" ';              
             }
             $out.=' class="clearfix iwt1-darkmenu-title" ';
             $out.=' href="';
             if($item->menu_children){
                $out.='#collapse';
                $out.=$item->ID;
            } else {
                $out.=$item->url;
            }
            $out.='" aria-expanded="true" 
            aria-controls="collapse'.$item->ID.'">
            <span class="pull-left">'.$item->title.'</span>
            <span class="iwt1-arrow pull-right"></span></a>';
            $out.=iwt_draw_mobmenu($items,$item->ID);
            $out.='</div>';
        } else {
            $out.='<li><a href="'.$item->url.'">'.$item->title.'</a></li>';
        }
    }
}
if($parent!=0 && isset($out)){
    return '<ul class="panel-collapse collapse iwt1-submenu-mobile" id="collapse'.$parent.'">'.$out.'</ul>';
}
return $out;
}

function iwt_draw_mobmenu_old($items, $parent = 0) {
    $return = false;
    if ($parent == 0) {
        $ulclass = 'iwt1-darkmenu-mobile';
        $liclass = 'iwt1-li-mobile';
    } else {
        $ulclass = 'iwt1-submenu-mobile';
        $liclass = '';
    }
    if ($parent == 0) {
        $out.="\n\t\t\t\t";
        $out.='<li class="iwt1-li-mobile iwt1-li-mobile-first clearfix">';
        $out.=iwt_auth_link();
        $out.='<button type="button" class="btn iwt1-orange-btn iwt1-orange-btn-mobile"';
        $out.= ' data-toggle="modal" data-target="#callme">Перезвоните мне</button>';
        $out.='</li>';
    }
    foreach ($items as $item) {
        if ($item->menu_item_parent == $parent) {
            $return = true;
            $out.="\n\t\t\t\t";
            $out.='<li class="' . $liclass . '">';
            if ($parent == 0) {
                $out.='<div class="clearfix iwt1-darkmenu-title">';
                $out.='<span class="pull-left"><a href="' . $item->url . '">' . $item->title . '</a></span>';
                if (!empty($item->menu_children)) {
                    $out.='<span class="iwt1-arrow pull-right"></span>';
                    $out.='</div>';
                }
            } else {
                $out.='<a href="' . $item->url . '">' . $item->title . '</a>';
            }
            $out.=iwt_draw_mobmenu($items, $item->ID);
            $out.='</li>';
        }
    }
    if (($return)) {

        $out = '<ul class="' . $ulclass . '">' . $out . '</ul>';
        return $out;
    }

    return;
}

function iwt_cartesian($input) {
    // filter out empty values
    $input = array_filter($input);

    $result = array(array());

    foreach ($input as $key => $values) {
        $append = array();

        foreach ($result as $product) {
            foreach ($values as $item) {
                $product[$key] = $item;
                $append[] = $product;
            }
        }

        $result = $append;
    }

    return $result;
}

function iwt_partition($list, $p) {
    $listlen = count($list);
    $partlen = floor($listlen / $p);
    $partrem = $listlen % $p;
    $partition = array();
    $mark = 0;
    for ($px = 0; $px < $p; $px++) {
        $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
        $partition[$px] = array_slice($list, $mark, $incr);
        $mark += $incr;
    }
    return $partition;
}

function iwt_auth_link() {
    if (!is_user_logged_in()) {
        $out.='<a href="#auth" data-target="#auth" data-toggle="modal" class="iwt1-li-mobile-bluelink">Проверить готовность</a>';
    } else {
        global $current_user;
        $user_name = $current_user->user_login;
        $dogovor = get_page_by_path($current_user->user_login, OBJECT, 'dogovor');

        $out.='<span class="iwt1-li-mobile-bluelink">Здравствуйте, <a href="' . home_url('/lk/') . '">' . $user_name . '</a>&nbsp;<a href="' . wp_logout_url(home_url()) . '" class="iwt1-lk-link">Выйти</a></span>';
    }
    return $out;
}

require(locate_template('includes/functions2.php'));

function iwt_draw_status($color, $text, $date = false) {
    $out = '<div class="iwt1-ready-' . $color . ' clearfix">
    <div class="pull-left iwt1-ready-icon iwt1-ready-icon-mobile"></div>
    <div class="pull-left iwt1-ready-text iwt1-col-small">
        ' . $text . '
    </div>';
    if ($date) {
        $out.='<div class="pull-left iwt1-ready-date">' . $date . '
    </div>';
}
$out.='</div>';
return $out;
}

function altair_comment($comment, $args, $depth) {

    $GLOBALS['comment'] = $comment;
    ?>
    <div class="iwt1-review-row row" id="li-comment-<?php comment_ID() ?>">
        <div class="iwt1-review-user clearfix">
            <div class="pull-left iwt1-userpic-wrapper altair-comment-avatar">

            </div>
            <div class="pull-left">
                <div class="iwt1-review-username">
                    <?php echo get_comment_author_link() ?>
                </div>
                <div class="iwt1-review-date">
                    <?php //echo get_comment_date('j F Y'); ?>
                </div>
            </div>
        </div>
        <div class="iwt1-review-text">
            <?php comment_text() ?>
        </div>


        <?php return;
        ?><div <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
        <div class="iwt1-review-row row">
            <div class="avatar-wrapper pull-left iwt1-userpic-wrapper">
                <div class="altair-comment-avatar">

                </div>                
            </div>
            <div class="pull-left">
                <div class="iwt1-review-username autor-comment">
                    <?php echo get_comment_author_link() ?>
                </div>
            </div>
            <div class="iwt1-review-text comment-text-altair" style="">                
                <?php comment_text() ?>
            </div>
        </div>
    </div>
    <?php
}


add_filter('comment_post_redirect', 'redirect_after_comment');

function redirect_after_comment($location) {
    $newurl = substr($location, 0, strpos($location, "#comment"));
    return $newurl . '?c=y';
}

function true_russian_date_forms($the_date = '') {
    if (substr_count($the_date, '---') > 0) {
        return str_replace('---', '', $the_date);
    }
    $replacements = array(
        "Январь" => "января",
        "Февраль" => "февраля",
        "Март" => "марта",
        "Апрель" => "апреля",
        "Май" => "мая",
        "Июнь" => "июня",
        "Июль" => "июля",
        "Август" => "августа",
        "Сентябрь" => "сентября",
        "Октябрь" => "октября",
        "Ноябрь" => "ноября",
        "Декабрь" => "декабря"
        );
    return strtr($the_date, $replacements);
}

add_filter('the_time', 'true_russian_date_forms');
add_filter('get_the_time', 'true_russian_date_forms');
add_filter('the_date', 'true_russian_date_forms');
add_filter('get_the_date', 'true_russian_date_forms');
add_filter('the_modified_time', 'true_russian_date_forms');
add_filter('get_the_modified_date', 'true_russian_date_forms');
add_filter('get_post_time', 'true_russian_date_forms');
add_filter('get_comment_date', 'true_russian_date_forms');
$theme->delete_option('styles');
