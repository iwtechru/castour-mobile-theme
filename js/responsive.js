
function showSubMenu(list, title, addClass)
{
    if (list.size() > 0)
    {
        if (!title.hasClass('open-menu'))
        {
            list.slideDown('fast', function(){
                title.addClass(addClass + ' open-menu').find('.iwt1-arrow').addClass('iwt1-arrow-active');
            });
        }
        else
        {
            list.slideUp('fast', function(){
                title.removeClass(addClass + ' open-menu').find('.iwt1-arrow').removeClass('iwt1-arrow-active');
            });
        }
    }
}




//на странице паспорта показывает и скрывает блоки
function showPriceForm()
{
    var variant = '';
    $('.iwt1-orange-form-btn-active').each(function(){
        variant = variant + '_' + $(this).data('variant');
    });

    $('.iwt1-price-result').addClass('hidden');
    $('.iwt_variant' + variant).removeClass('hidden');


    $('.iwt1-price-docs-list').addClass('hidden');
    $('.doc' + variant).removeClass('hidden');
}





$(document).ready(function(){


    $(document).on('click touchend', '.iwt1-toggle-btn', function(){
        $(this).toggleClass('iwt1-toggle-active');
    });

//
//УБРАТЬ!
//    $(document).on('click touchend', '.iwt1-orange-title', function(){
//        var list = $(this).closest('.iwt1-white-border').find('.iwt1-list');
//        showSubMenu(list, $(this), 'iwt1-orange-underlined');
//    });




//ИЗМЕНЕНИЯ для черного меню УДАЛИТЬ
//    $(document).on('click touchend', '.iwt1-darkmenu-title', function(){
////        if (!$(this).hasClass('open-menu') && $('#show-menu').find('.open-menu').size() > 0)
////        {
////            return;
////        }
////        else
////        {
//            var list = $(this).closest('.iwt1-li-mobile').find('.iwt1-submenu-mobile');
//            showSubMenu(list, $(this), '');
////        }
//    });
//ИЗМЕНЕНИЯ для черного меню удалить



//ИЗМЕНЕНИЯ эти строки добавить, это смена стрелки
    $(document).on('shown.bs.collapse', '.iwt1-submenu-mobile', function(){
        $(this).siblings('.iwt1-darkmenu-title').find('.iwt1-arrow').addClass('iwt1-arrow-active');
    });

    $(document).on('hide.bs.collapse', '.iwt1-submenu-mobile', function(){
        $(this).siblings('.iwt1-darkmenu-title').find('.iwt1-arrow').removeClass('iwt1-arrow-active');
    });




    //смена стрелки у оранжевого
    $(document).on('shown.bs.collapse', '.iwt1-submenu-index', function(){
        $(this).siblings('.iwt1-orange-title').addClass('open');
        $(this).siblings('.iwt1-orange-title').find('.iwt1-arrow').addClass('iwt1-arrow-active');
    });


    $(document).on('hide.bs.collapse', '.iwt1-submenu-index', function(){
        $(this).siblings('.iwt1-orange-title').removeClass('open');
        $(this).siblings('.iwt1-orange-title').find('.iwt1-arrow').removeClass('iwt1-arrow-active');
    });


//ИЗМЕНЕНИЯ эти строки добавить, это смена стрелки



//раздвигает подменю на странице справки
    $(document).on('click touchend', '.iwt1-faq-list-link', function(e){
        e.preventDefault();
        var list = $(this).closest('li').find('.iwt1-faq-sublist');
        showSubMenu(list, $(this), '');
    });


//показывает полный текст в подменю на странице справки
    $(document).on('click touchend', '.iwt1-faq-list-sublink', function(e){
        e.preventDefault();
        var li = $(this).closest('li');
        var block = li.find('.iwt1-faq-block');
        li.toggleClass('active');
        showSubMenu(block, $(this), '');
    });


//пролистывание страницы к форме отзывов
    $('#iwt1-add-review').on('click touchend', function(){
        $('html, body').animate({
            scrollTop: $(".iwt1-review-form-wrapper").offset().top
        }, 2000);
    });


/*ИЗМЕНЕНИЯ!!   это удалить*/
    //при клике 
//  $(document).on('touchend', '.iwt1-orange-form-btns', function(e){
//      $(this).closest('.iwt1-passport-row').find('.iwt1-orange-form-btns').removeClass('iwt1-orange-form-btn-active');
//      $(this).addClass('iwt1-orange-form-btn-active');
//      showPriceForm();
//  });


/*ИЗМЕНЕНИЯ!!   это добавить*/
    $('.iwt1-yellow-block').on('shown.bs.collapse', '.iwt1-orange-hidden', function(){
        var variant = $(this).data('variant');
        console.log(variant);
        var activeLink = $('.iwt1-orange-form').find('a[href="#' + variant + '"]');
        activeLink.closest('.iwt1-passport-row').find('.iwt1-orange-form-btns').removeClass('iwt1-orange-form-btn-active');
        $('.iwt1-orange-hidden').removeClass('in');
        activeLink.addClass('iwt1-orange-form-btn-active');
        showPriceForm();
    });

    //при загрузке
    showPriceForm();



    $(document).on('click touchend', '#cmdReset', function(){
        cleanForm();
    });



    $(document).on('change', '#TxtField', function(){
        settingDate('TxtField');
    });

    $(document).on('click touchend', '#switch1', function(e){
        e.preventDefault();
        switch1();

    });

    $(document).on('click touchend', '#switch2', function(e){
        e.preventDefault();
        switch2();
    });

    $(document).on('click touchend', '#iwt1-add-visa-dates-row', function(){
        addToFromDate();
    });

    $(document).on('click touchend', '#iwt1-add-passport-dates-row', function(){
        addTxtPassport();
    });



    $(document).on('click touchend', '#cmdCalculate', function(){
        fncCalculate();
        switch1();
    });

    $(document).on('click touchend', '#checkboxG1', function(){
        $('#altVisa').click();
        $('#checkboxG2').prop('checked', false);
    });

    $(document).on('click touchend', '#checkboxG2', function(){
        $('#altControl').click();
        $('#checkboxG1').prop('checked', false);
    });


    $(document).on('click touchend', '#not_random', function(){
        fncPassport(true);
    });

    $(document).on('click touchend', '#random', function(){
        fncPassport(false);
    });







});

function switch1(){
    fncPassport(true);
}

function switch2(){
    fncPassport(false);
}
