$(document).ready(function () {
//    этот блок нужен только для того, чтоб показать модальные окна
    var hash = window.location.hash;
    if (hash !== '')
    {
        if (hash == '#auth')
        {
            $('#auth').modal('show');
        }

        if (hash == '#recovery')
        {
            $('#recovery').modal('show');
        }
    }


    $('input, textarea').placeholder();

//а это обязательно для меню, потому что на чистом css не удалось сделать
    $('.iwt1-li').on('mouseover', function () {
        $('.iwt1-submenu-icon').removeClass('hidden');
        $('.iwt1-submenu').addClass('hidden');
        $('.' + $(this).data('target')).removeClass('hidden');
        $('.iwt1-li').removeClass('active').addClass('current');
        $(this).addClass('active');
    });

//курсор вне белого подменю
    $('.iwt1-submenu').on('mouseleave', function () {
        closeSubmenu();
    });



    $('.iwt1-menu-wrapper').on('click touchend', '.iwt1-submenu-icon', function (e) {
        e.preventDefault();
        $(this).closest('.iwt1-submenu').find('.iwt1-submenu-inner').slideDown();
        $(this).closest('.iwt1-submenu').addClass('iwt1-submenu-open');
        ///это только если нечего подгружать
        $(this).addClass('hidden');
    });




    /**
     * клик на странице, чтоб закрыть обратный звонок
     */

    $(document).on('click touchend', 'body', function (e) {
        var wrapper = $('.iwt1-submenu');

        if (!wrapper.is(e.target)
                && wrapper.has(e.target).length === 0)
        {
            closeSubmenu();
        }
    });

    $('#js-translit').on('click touchend', function () {
        var lastname = $('#iwt1-translit-lastname').val(),
                name = $('#iwt1-translit-name').val();

        if (lastname === '')
        {
            lastname = 'Фамилия';
        }
        if (name === '')
        {
            name = 'Имя';
        }

        $('#iwt1-lastname-rus').html(lastname);
        $('#iwt1-lastname-translit').html(translite(lastname));


        $('#iwt1-name-rus').html(name);
        $('#iwt1-name-translit').html(translite(name));

//этот кусок утащила из старого скрипта
        var
                k = "XXXXXXXXXXRUSXXXXXXXXXXXXXXXXXXXXXXXX <<<<<<<<<<<<<< 15",
                l = "р&ltrus" + translite(lastname) + "&lt&lt" + translite(name) + "&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt&lt",
                g = translite(lastname),
                f = translite(name),
                i = "р&ltrus" + g + "&lt&lt" + f;
        $('#iwt1-passpost-bottom').html(i);
        for (var a = "", e = (544 - parseInt($("#iwt1-passpost-bottom").width())) / 9, l = 0; e > l; l++) {
            a += "&lt";
        }
        $('#iwt1-passpost-bottom').html(i + a + "<br />" + k);
    });


    $('#iwt1-show-rules').on('click touchend', function () {
        $('.iwt1-translit-passport').addClass('hidden');
        $('.iwt1-translit-rules').removeClass('hidden');
    });

    $('.iwt1-translit-wrapper').on('click touchend', '.iwt1-close-rules', function (e) {
        e.preventDefault();
        $('.iwt1-translit-passport').removeClass('hidden');
        $('.iwt1-translit-rules').addClass('hidden');
    });

});


function translite(str) {
    str=str.replace('ь','');
    str=str.replace('Ь','');
    var arr = {'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd',
        'е': 'e', 'ё': 'e', 'ж': 'zh', 'з': 'z', 'и': 'i', 'й': 'i',
        'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p',
        'р': 'r', 'с': 's', 'т': 't', 'у': 'u',
        'ф': 'f', 'х': 'kh', 'ц': 'ts', 'ч': 'ch', 'ш': 'sh',
        'щ': 'shch', 'ы': 'y', 'ь': '', 'ъ': 'ie', 'э': 'e', 'ю': 'iu',
        'я': 'ia',
        'А': 'A', 'Б': 'B', 'В': 'V',
        'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'E',
        'Ж': 'ZH', 'З': 'Z', 'И': 'I', 'Й': 'I', 'К': 'K', 'Л': 'L',
        'М': 'M', 'Н': 'N', 'О': 'O', 'П': 'P', 'Р': 'R', 'С': 'S',
        'Т': 'T', 'У': 'U', 'Ф': 'F', 'Х': 'KH', 'Ц': 'TS',
        'Ч': 'CH', 'Ш': 'SH', 'Щ': 'SHCH', 'Ы': 'Y', 'Ь': '', 'Ъ': 'IE',
        'Э': 'E', 'Ю': 'IU', 'Я': 'IA','Ь':''
    };
    var replacer = function (a) {
        return arr[a] || a
    };
    return str.replace(/[А-яёЁЬ]/g, replacer)
}

//закрытие подменю при выходе курсора из меню или клике на боди
function closeSubmenu()
{
    $('.iwt1-submenu-inner').slideUp('fast');
    $('.iwt1-submenu').addClass('hidden').removeClass('iwt1-submenu-open');
    $('.iwt1-li').removeClass('active').addClass('current');

    $('.iwt1-submenu-icon').removeClass('hidden');
}