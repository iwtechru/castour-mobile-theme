jQuery(document).ready(function () {
    if(jQuery('.page-template-tpl-oformlenie').length){
        jQuery('.iwt1-form-info-li').click(function(){
            if(jQuery('.liclicked').length){return;}
            if (this === event.target) {
                jQuery('body').addClass('liclicked');
                jQuery(this).find('a').click();
                setTimeout(function(){
                    jQuery('body').removeClass('liclicked');
                });
            }
        });
        ii=0;
        setTimeout(function(){
            jQuery('.iwt1-orange-form .iwt1-passport-row').each(function(){
                jQuery(this).find('.iwt1-orange-form-btns').eq(iwtcp[ii]-1).click();
                ii++;
            });
        },50);
    }
    jQuery('.iwt1-check-title a').click(function(){
     $("html, body").animate({ scrollTop: 0 }, "slow");
 });
    if (jQuery('.page-template-tpl-otzivi2').length) {        
       jQuery('.iwt1-review-input,.logged-in-as').after('<div class="iwt1-review-photo-row clearfix"><div class="iwt1-review-label pull-left">Добавить фото:</div><div class="iwt1-review-fileinput-wrapper pull-left"><span class="btn-file">                Прикрепить файл</span></div></div>');
       jQuery('.btn-file').append(jQuery('#attachment'));
       jQuery('.iwt1-review-photo-row').after('<div class="iwt1-review-hint-row">' + jQuery('.attachmentRules').text() + '</div>');
       jQuery('[for="attachment"]').hide();

       $ccount = 0;
       jQuery('.iwt1-review-row').each(function () {
        $ccount++;
        $this = jQuery(this);
        $this.attr('data-comm', $ccount);
        if (!($this.find('.attachmentFile p img').length)) {
            return;
        }
        $this.find('.altair-comment-avatar').html($this.find('.attachmentFile p').html());
        $this.find('.altair-comment-avatar img').addClass('img-responsive').addClass('img-circle');
        $this.find('.attachmentFile').remove();
    });
       jQuery('.iwt1-review-row').hide();
       jQuery('.iwt1-review-row:hidden').slice(-5).show();
       jQuery('#iwt_show_comments').click(function () {
        jQuery('.iwt1-review-row:hidden').slice(-5).show();
        if (!jQuery('.iwt1-review-row:hidden').length) {
            jQuery('#iwt_show_comments').remove();
        }
    });

   }
   if (jQuery('.page-template-tpl_company').length) {
    jQuery('.iwt1-text-16 h2:first').each(function () {
        $this = jQuery(this);
        jQuery('.iwt1-text-light').before('<div class="iwt1-sertificate-title iwt1-margin-bottom-30">' + $this.text() + '</div>');
        $this.remove();
    });
        // jQuery('ul:first').addClass('iwt1-doc-list iwt1-company-info-list iwt1-margin-top-30');
        jQuery('.iwtcontent2 ul').addClass('iwt1-doc-list iwt1-company-info-list iwt1-margin-top-30');
        jQuery('.iwtcontent3 ul').addClass('iwt1-doc-list iwt1-company-info-list iwt1-company-info-list-margin');
        jQuery('.iwtcontent4').each(function () {
            jQuery(this).find('h2').each(function () {
                jQuery(this).after('<div class="iwt1-company-subtitle iwt1-margin-bottom-20">' + jQuery(this).text() + '</div>').remove();
            });
        });
        jQuery('.iwtcontent3').each(function () {
            jQuery(this).find('h2').each(function () {
                jQuery(this).after('<div class="iwt1-company-subtitle iwt1-margin-bottom-20">' + jQuery(this).text() + '</div>').remove();
            });
        });
        jQuery('.iwt1-text-light').each(function () {
            $this = jQuery(this);
            $this.find('p').each(function () {
                if (jQuery(this).html() === '&nbsp;') {
                    jQuery(this).remove();
                }
            });
            $this.find('.iwt_rednumber').not(':last').closest('p').addClass('iwt1-margin-bottom-0');
            $this.find('.iwt_rednumber').each(function () {
                jQuery(this).after('<b class="iwt1-orange-text">' + jQuery(this).text() + '</b>');

                jQuery(this).remove();

            });
            jQuery('h2:first').before('<div class="iwt1-sertificate-title">' + jQuery('h2:first').text() + '</div>').remove();
        });
    }
    jQuery('.page-template-tpl-contacts-php h2').each(function () {
        $this = jQuery(this);
        $this.addClass('iwteliminate');
        $this.after('<div class="iwt1-contact-title">' + $this.text() + '</div>');
        $this.remove();
    });
    jQuery('.page-template-tpl-contacts-php h3').each(function () {
        $this = jQuery(this);
        $this.addClass('iwteliminate');
        $this.after('<div class="iwt1-contact-subtitle">' + $this.text() + '</div>');
        $this.remove();
    });
});
jQuery(document).ready(function () {
    jQuery('#ninja_forms_form_5_all_fields_wrap input[type="text"]').each(function () {
        $this = jQuery(this);
        $this.addClass('iwt1-input-mobile');        
        $this.val('');
        $this.wrap('<div class="iwt1-input-wrapper"></div>');
    });
    $('#ninja_forms_field_7').attr('type','tel');
});