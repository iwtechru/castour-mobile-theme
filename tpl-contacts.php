<?php
global $theme;
get_header();
while (have_posts()) {
    the_post();
    ?>
    <div class="container-fluid">
        <div class="iwt1-pagetitle iwt1-pagetitle-contacts">
            Контакты
        </div>
        <div class="clearfix">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 iwt1-address-block">
                <div class="iwt1-contact-icon"></div>
                <div class="iwt1-contact-text text-center">
                    <span class="iwt1-contact-text-semibold"><?php echo $theme->field('Текст справа от флага', array('notsingle' => false, 'variant' => 'textarea')); ?>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 iwt1-time-block">
                <div class="iwt1-contact-icon iwt1-contact-icon-2"></div>
                <div class="iwt1-contact-text text-center">
                    <?php echo $theme->field('Текст справа от часов', array('notsingle' => false, 'variant' => 'textarea')); ?>
                </div>
            </div>


            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 iwt1-contact-phone-block">
                <div class="iwt1-contact-icon iwt1-contact-icon-3"></div>
                <div class="iwt1-contact-text text-center">
                    <?php echo $theme->field('Текст справа от трубки', array('notsingle' => false, 'variant' => 'textarea')); ?>
                </div>
            </div>


            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 iwt1-contact-email-block">
                <div class="iwt1-contact-icon iwt1-contact-icon-4"></div>
                <div class="iwt1-contact-text text-center">
                    <?php echo $theme->field('Текст справа от конверта', array('notsingle' => false, 'variant' => 'textarea')); ?>
                </div>
            </div>
        </div>
        <?php
        $content = explode('<hr />', get_the_content());

        $arr = array('Схема', 'Панорама');
        ?>
        <div class="iwt1-text iwt1-text-16 iwt1-text-light iwt1-text-contacts">
            <?php echo $content[0]; ?>
        </div>


        <div class="iwt1-map-wrapper">

            <ul class="iwt1-contacts-list clearfix">
                <li class="active">
                    <a data-toggle="tab" href="#shema" aria-expanded="true">
                        Схема
                        <span class="iwt1-contacts-triangle"></span>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panorama" aria-expanded="false" >
                        Панорама
                        <span class="iwt1-contacts-triangle"></span>
                    </a>
                </li>
            </ul>
            <div class="row">
                <?php $arr = array('Схема', 'Панорама'); ?>
                <div class="tab-content">
                    <div id="shema" class="tab-pane fade active in">
                        <?php echo $theme->field($arr[0], array('notsingle' => false, 'variant' => 'textarea')); ?>
                    </div>
                    <div id="panorama" class="tab-pane fade">
                        <?php echo $theme->field($arr[1], array('notsingle' => false, 'variant' => 'textarea')); ?>
                    </div>
                </div>
            </div>
        </div>        
        <div class="iwt1-text iwt1-text-16 iwt1-text-light iwt1-text-howto-go">
            <?php echo $content[1]; ?>
        </div>

    </div>
    <?php
}
get_footer();
