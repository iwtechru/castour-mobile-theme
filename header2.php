<?php global $theme; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Кастур - помощь в оформлении загранпаспорта | Компания Кастур - надежная помощь в сложном деле</title>
        <link rel="apple-touch-icon" sizes="152x152" href="css/img/icons/152152.png">
        <link rel="apple-touch-icon" sizes="120x120" href="css/img/icons/120120.png">
        <link rel="apple-touch-icon" sizes="48x48" href="css/img/icons/4848.png">
        <link rel="apple-touch-icon" sizes="16x16" href="css/img/icons/1616.png">
        <link rel="icon" type="image/png" href="css/img/icons/1616.png" sizes="16x16">
        <link rel="icon" type="image/png" href="css/img/icons/4848.png" sizes="48x48">
        <link rel="icon" type="image/png" href="css/img/icons/120120.png" sizes="120x120">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,600,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <!--<link href='http://fonts.googleapis.com/css?family=PT+Sans&amp;subset=cyrillic' rel='stylesheet' type='text/css'>-->



        <!--[if lt IE 9]>
                  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
          <![endif]-->


        <!--[if gte IE 9]>
            <style type="text/css">
             *,*:hover, *:active, *:focus, *:visited{
                 filter: none;
              }
            </style>
        <![endif]-->   
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class();?>>

        <!--        <div class="clearfix iwt1-lk-top">
                    <div class="wrap">
                        <a href="#" class="pull-right iwt1-lk-link">Выйти из кабинета</a>
                    </div>
                </div>-->

        <?php get_template_part('block','alert');?>
        <nav class="navbar  iwt1-navbar" role="navigation">
            <div class="container-fluid">
                <div class="row">
                    <div class="navbar-header navbar-header-bordered">
                        <a class="iwt1-logo-mobile" href="<?php echo get_site_url();?>">
                            <img src="<?php bloginfo('template_url'); ?>/css/img/logo2.png" alt="" class="img-responsive"/>
                        </a>
                        <button type="button" class="navbar-toggle iwt1-toggle-btn pull-right" 
                                data-toggle="collapse" data-target="#show-menu">
                            <span class="iwt1-toggle-icon"></span>
                        </button>
                    </div>
                     <div class="collapse navbar-collapse" id="show-menu">


                            <div class="iwt1-darkmenu-mobile">                             
                              <!--блок самый первый, тут без ид и разворота-->
                              <div class="iwt1-li-mobile iwt1-li-mobile-first clearfix">
                                <?php echo iwt_auth_link() ;?>
                                <button type="button" class="btn iwt1-orange-btn iwt1-orange-btn-mobile" 
                                data-toggle="modal" data-target="#callme">Перезвоните мне</button>
                            </div>
                            <!--блок самый первый, тут без ид и разворота-->

                            <?php
                            $items = ($theme->menu_items('Верхнее меню (iwt-mob)'));
                            echo iwt_draw_mobmenu($items);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>


