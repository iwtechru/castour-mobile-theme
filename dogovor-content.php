<?php
global $theme;
$email = types_render_field("dogovor_e_mail", array('output' => 'raw'));
$notification_shedule = wp_next_scheduled('iwto_passport_ready_event', array($email));
if (isset($_POST['notify_email'])) {
    $email = types_render_field("dogovor_e_mail", array('output' => 'raw'));
}
if (isset($_POST['notify_email'])) {
    $email = $_POST['notify_email'];
    if (isset($_POST['notify_me']) && isset($_POST['email_date'])) {
        if ($_POST['email_date'] > time()) {
            $result = wp_schedule_single_event($_POST['email_date'], 'iwto_passport_ready_event', array($email));
            update_post_meta(get_the_ID(), 'wpcf-dogovor_e_mail', $email);
            update_post_meta(get_the_ID(), 'dogovor_is_send_email', 1);
            if ($result === false) {
                echo 'Не удалось добавить уведомление в расписание!';
            }
        }
    } else {
        wp_unschedule_event($notification_shedule, 'iwto_passport_ready_event', array($email));
        update_post_meta(get_the_ID(), 'dogovor_is_send_email', 2);
    }
    $notification_shedule = wp_next_scheduled('iwto_passport_ready_event', array($email));
}
?>
<div class="container-fluid">
    <form  action="" method="POST">
        <div class="iwt1-pagetitle iwt1-pagetitle-lk">
            Личный кабинет
        </div>
        <!--форма уведомлений в личном кабинете-->

        <div class="iwt1-formtext">
            Ваш номер договора: <b><?php echo types_render_field("dogovor_num", array()); ?></b>
        </div>

        <div class="iwt1-notification-form clearfix">
            <div class="pull-left iwt1-checkbox-wrapper">
                <input type="checkbox" id="iwt1-checkbox"  name="notify_me"  class="iwt1-checkbox-mobile" onclick="submit()" <?php
                if ($notification_shedule == false) {                 ;
                } else {
                    ?> checked <?php } ?>>
                    <label for="iwt1-checkbox" class="iwt1-label-mobile">Уведомить о готовности паспорта по почте</label>
                </div>
            </div>
            <div class="iwt1-input-email-wrapper">
                <input type="text" name="notify_email" class="iwt1-input iwt1-lk-email" placeholder="e-mail" value="<?php
                if (isset($_POST['notify_email'])) {
                    echo $_POST['notify_email'];
                } else {
                    echo types_render_field("dogovor_e_mail", array('output' => 'raw'));
                }
                ?>"/>
            </div>
            <!--форма уведомлений в личном кабинете-->

            <?php
            $child_posts = types_child_posts('dogovor_passport');
            $email_date = time() + 60 * 60 * 24 * 30;
            ?>
            <div class="row">
                <?php
                foreach ($child_posts as $child_post) {

                    $ready_date = $child_post->fields['dogovor_passport_date2'];
                    $dogovor_state = get_dogovor_state($child_post->fields['dogovor_passport_date1'], $ready_date, $child_post->fields['dogovor_passport_date3']);
                    if ($child_post->fields['dogovor_passport_freeze'] === '1') {
                        $dogovor_state = -1;
                    }
                    ?>
                    <!--блок с информацией о готовности заказа-->
                    <div class="iwt1-ready-row clearfix">
                        <div class="iwt1-formtext iwt1-margin-bottom-40">
                            Клиент: <b><?php echo $child_post->fields['dogovor_passport_fio'] ?></b>
                        </div>
                        <div class="clearfix">
                            <?php
                            if ($dogovor_state <= 0) {
                                echo iwt_draw_status('orange', 'Подача документов');
                            } else {
                                echo iwt_draw_status('green', 'Документы поданы');
                            }
                            ?>
                            <div class="iwt1-ready-arrow-mobile"></div>
                            <?php
                            if ($dogovor_state < 2) {
                                echo iwt_draw_status('gray', 'Проверка поданных документов');
                            } elseif ($dogovor_state == 2) {
                                echo iwt_draw_status('orange', 'Проверка поданных документов');
                            } else {
                                echo iwt_draw_status('green', 'Проверки пройдены');
                            }
                            ?>
                            <div class="iwt1-ready-arrow-mobile"></div>
                            <?php
                            if ($dogovor_state < 4) {
                                echo iwt_draw_status('gray', 'Готовность паспорта');
                            } elseif ($dogovor_state == 4) {
                                echo iwt_draw_status('orange', 'Готовность паспорта');
                            } else {
                                echo iwt_draw_status('green', 'Паспорт готов', date('d.m.Y', $ready_date));
                                if ($ready_date < $email_date) {
                                    $email_date = $ready_date;
                                }
                            }
                            ?>
                        </div>
                        <div class="iwt1-ready-text-wrapper">
                            <?php if ($dogovor_state < 0) { ?>
                            <p>Чтобы узнать статус вашей заявки или дату готовности, просьба звонить по номеру<br>
                                <?php echo $theme->sitevar('Телефон №1 в шапке'); ?></p>
                                <?php } else { ?>
                                <p>Готовность паспорта <?php echo date('d.m.Y', $ready_date); ?> </p>
                                <?php if(2==1){ ?>      <p>Забор паспорта - в день готовности после 15:00, в последующие дни в любое время.</p><?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                        <!--блок с информацией о готовности заказа.конец-->
                        <?php } ?>
                    </div>
                    <!--блок со списком документов-->
                    <div class="iwt1-docs-wrapper">
                        <div class="iwt1-docs-block">
                            <div class="iwt1-padding-block iwt1-docs-border">
                                <div class="iwt1-docs-title">При себе иметь:</div>
                                <div class="iwt1-docs-subtitle">Для взрослых</div>
                                <ul class="iwt1-docs-list">
                                    <?php
                                    $doc_option = $theme->sitevar('Список документов договора для взрослых. Перечислить через точку с запятой');
                                    $docs = explode(';', $doc_option);
                                    foreach ($docs as $doc) {
                                        echo '<li>' . $doc . '</li>';
                                    }
                                    ?>
                                </ul>
                            </div>

                            <div class="iwt1-padding-block">
                                <div class="iwt1-docs-subtitle">Для детей</div>
                                <ul class="iwt1-docs-list">
                                    <?php
                                    $doc_option = $theme->sitevar('Список документов договора для детей. Перечислить через точку с запятой');
                                    $docs = explode(';', $doc_option);
                                    foreach ($docs as $doc) {
                                        echo '<li>' . $doc . '</li>';
                                    }
                                    ?>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <!--блок со списком документов-->




                    <!--блок с расписанием и адресом-->       
                    <?php $address_id = wpcf_pr_post_get_belongs($post->ID, 'fms_address');
                    $address = get_post($address_id);?>
                    <div class="iwt1-address-col">
                        <div class="iwt1-docs-title">Адрес</div>
                        <div class="iwt1-formtext iwt1-margin-bottom-50">
                            <?php echo !empty($address->post_content) ? $address->post_content : $address->post_title?>
                        </div>
                        <div class="iwt1-docs-title">Режим работы</div>
                        <div class="iwt1-time-block">
                            <div class="iwt1-time-row clearfix">
                                <div class="col-xs-4 iwt1-col-day">
                                    Понедельник
                                </div>
                                <div class="col-xs-4 iwt1-col-time">
                                    <?php echo types_render_field("fms_address_mon", array("post_id"=>$address_id, "output"=>"html"));?>
                                </div>
                                <div class="col-xs-4 iwt1-col-orange">
                                    <?php echo types_render_field("fms_address_mon_lunch", array("post_id"=>$address_id, "output"=>"html"));?>
                                </div>
                            </div>
                            <div class="iwt1-time-row clearfix">
                                <div class="col-xs-4 iwt1-col-day">
                                    Вторник
                                </div>
                                <div class="col-xs-4 iwt1-col-time">
                                    <?php echo types_render_field("fms_address_tue", array("post_id"=>$address_id, "output"=>"html"));?>
                                </div>
                                <div class="col-xs-4 iwt1-col-orange">
                                    <?php echo types_render_field("fms_address_tue_lunch", array("post_id"=>$address_id, "output"=>"html"));?>
                                </div>
                            </div>
                            <div class="iwt1-time-row clearfix">
                                <div class="col-xs-4 iwt1-col-day">
                                    Среда
                                </div>
                                <div class="col-xs-4 iwt1-col-time">
                                    <?php echo types_render_field("fms_address_wed", array("post_id"=>$address_id, "output"=>"html"));?>
                                </div>
                                <div class="col-xs-4 iwt1-col-orange">
                                    <?php echo types_render_field("fms_address_wed_lunch", array("post_id"=>$address_id, "output"=>"html"));?>
                                </div>
                            </div>
                            <div class="iwt1-time-row clearfix">
                                <div class="col-xs-4 iwt1-col-day">
                                    Четверг
                                </div>
                                <div class="col-xs-4 iwt1-col-time">
                                    <?php echo types_render_field("fms_address_thu", array("post_id"=>$address_id, "output"=>"html"));?>
                                </div>
                                <div class="col-xs-4 iwt1-col-orange">
                                    <?php echo types_render_field("fms_address_thu_lunch", array("post_id"=>$address_id, "output"=>"html"));?>
                                </div>
                            </div>
                            <div class="iwt1-time-row clearfix">
                                <div class="col-xs-4 iwt1-col-day">
                                    Пятница
                                </div>
                                <div class="col-xs-4 iwt1-col-time">
                                    <?php echo types_render_field("fms_address_fri", array("post_id"=>$address_id, "output"=>"html"));?>
                                </div>
                                <div class="col-xs-4 iwt1-col-orange">
                                    <?php echo types_render_field("fms_address_fri_lunch", array("post_id"=>$address_id, "output"=>"html"));?>
                                </div>
                            </div>
                            <div class="iwt1-time-row clearfix">
                                <div class="col-xs-4 iwt1-col-day">
                                    Суббота
                                </div>
                                <div class="col-xs-4 iwt1-col-time">
                                    <?php echo types_render_field("fms_address_sat", array("post_id"=>$address_id, "output"=>"html"));?>
                                </div>
                                <div class="col-xs-4 iwt1-col-orange">
                                    <?php echo types_render_field("fms_address_sat_lunch", array("post_id"=>$address_id, "output"=>"html"));?>
                                </div>
                            </div>
                            <div class="iwt1-time-row clearfix">
                                <div class="col-xs-4 iwt1-col-day">
                                    Воскресенье
                                </div>
                                <div class="col-xs-4 iwt1-col-time">
                                    <?php echo types_render_field("fms_address_sun", array("post_id"=>$address_id, "output"=>"html"));?>
                                </div>
                                <div class="col-xs-4 iwt1-col-orange">
                                    <?php echo types_render_field("fms_address_sun_lunch", array("post_id"=>$address_id, "output"=>"html"));?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--блок с расписанием и адресом-->
                    <input type="hidden" name="email_date" id="email_date" value="<?php echo $child_post->fields['dogovor_passport_date3'] + 8 * 60 ^ 2; ?>"/>
                </form>
            </div>
