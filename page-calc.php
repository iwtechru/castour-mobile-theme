<?php get_header(); global $theme;?>
    <div class="container-fluid">
        <div class="iwt1-pagetitle iwt1-pagetitle-visa">
            Визовый калькулятор
        </div>
        <a href="<?php echo $theme->sitevar('Ссылка блока цены и сроки'); ?>" class="btn iwt1-gray-btn iwt1-gray-btn-small">цены и сроки</a>


        <div class="iwt1-text iwt1-text-16 iwt1-text-light">
            <p>
                Получившим однократную шенгенскую визу пока еще рано делать эту
                страницу нашего сайта своей «настольной книгой». Можете беззаботно
                отправляться в путешествие по Европе. А вот если вы являетесь
                счастливым обладателем мультивизы, то для вас калькулятор шенгенских виз,
                который мы предусмотрительно приготовили — лучший друг и помощник.
                А как же иначе рассчитать, сколько времени вы имеете право пребывать
                в странах Шенгенского соглашения, и впустят ли вас бдительные пограничники?
            </p>
        </div>


        <div class="iwt1-control-date-block">
            Предлагаемая дата въезда/Контрольная дата:

            <div class="iwt1-control-date-wrapper clearfix">
                <input type="text" value="" class="iwt1-control-date datepicker_expected" placeholder="17/04/15"
                       id="TxtField" name="TxtField">
            </div>
        </div>

        <div class="clearfix">
            <div class="iwt1-plan-radio-wrapper">
                <input type="radio" id="checkboxG1" class="iwt1-checkbox-mobile" checked="checked">
                <label for="checkboxG1" class="iwt1-label-mobile">Планирование</label>
            </div>
            <div class="iwt1-control-radio-wrapper">
                <input type="radio" id="checkboxG2" class="iwt1-checkbox-mobile">
                <label for="checkboxG2" class="iwt1-label-mobile">Контроль</label>
            </div>
        </div>


        <ul class="iwt1-visa-btns-list clearfix">
            <li class="active">
                <a href="#" id="switch1">
                    по поездкам
                </a>
            </li>
            <li>
                <a href="#" id="switch2">
                    произвольно
                </a>
            </li>
        </ul>

        <div class="tab-content clearfix">
            <div id="stays_wrap" class="tab-pane1 iwt1-tab">
                <div class="iwt1-tab-title">Предыдущие поездки</div>
                <div class="stays" id="stays"></div>
                <button type="button" class="iwt1-gray-btn iwt1-add-visa-dates-row" id="iwt1-add-visa-dates-row">
                    добавить
                </button>
            </div>


            <div id="passportstays_wrap" class="tab-pane1 iwt1-tab">
                <div class="iwt1-tab-title">Все даты предыдущих поездок</div>

                <div id="passportstays"></div>
                <button type="button" class="iwt1-gray-btn iwt1-add-visa-dates-row" id="iwt1-add-passport-dates-row">
                    добавить
                </button>
            </div>
        </div>


        <div class="row iwt1-visa-btns-wrapper">
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 iwt1-col-calculate-btn">
                <button type="button" class="iwt1-orange-btn iwt1-orange-btn-block iwt1-visa-btns"
                        id="cmdCalculate">подсчитать
                </button>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 iwt1-col-clear">
                <button type="button" class="iwt1-gray-btn iwt1-visa-btns" id="cmdReset">сбросить</button>
            </div>
        </div>


        <div class="iwt1-visa-calculation" id="resultWrapper">
            <div class="iwt1-visa-calculation-text" id="result">
            </div>
            <div class="iwt1-visa-calculation-bottom"></div>
        </div>


        <div>
            <div class="mode_control">
                <input type="radio" class="" name="group1" value="altVisa" id="altVisa" checked=""/>
                <label for="altVisa" class="">Планирование</label>
                <br/>
                <input type="radio" class="" name="group1" value="altControl" id="altControl"/>
                <label for="altControl" class="">Контроль</label>
            </div>
            <div class="mode_control second">
                <input type="radio" class="" name="view" value="week" id="not_random" checked=""/>
                <label for="not_random" class="">По поездкам</label>
                <br/>
                <input type="radio" class="" name="view" value="month" id="random"/>
                <label for="random" class="">Произвольно</label>
            </div>
        </div>


        <div class="iwt1-visa-instruction">
            Инструкция по использованию калькулятора - <a href="<?php
            $link=$theme->sitevar('Визовый калькулятор: инструкция');
            if(!strlen($link)){?>/wp-content/uploads/2014/12/rukovodstvo-polzovatelya-po-shengenskomu-kalkulyatoru.pdf<?php }
            else {echo $link;}?>">скачать здесь</a>
        </div>


        <div class="iwt1-visa-title">
            Зачем нужен визовый калькулятор
        </div>
        <div class="iwt1-text iwt1-text-16 iwt1-text-light">

            <p>Получившим однократную шенгенскую визу пока еще рано делать эту
                страницу нашего сайта своей «настольной книгой». Можете беззаботно
                отправляться в путешествие по Европе. А вот если вы являетесь счастливым
                обладателем мультивизы, то для вас калькулятор шенгенских виз, который мы
                предусмотрительно приготовили — лучший друг и помощник. А как же иначе рассчитать,
                сколько времени вы имеете право пребывать в странах
                Шенгенского соглашения, и впустят ли вас бдительные пограничники?
            </p>
        </div>

        <div class="iwt1-visa-title iwt1-margintop-55">
            В чем разница
        </div>
        <div class="iwt1-text iwt1-text-16 iwt1-text-light">
            <p>Раньше дни отсчитывались вперед от даты первого въезда. 180 дней
                заканчивались, и происходило обнуление
                вашего «счета». Можно было снова въезжать хоть сразу на 90 дней.
            </p>

            <p>А вот теперь и 180, и 90 дней отсчитываются назад, каждый раз при въезде.
                Стартовая точка, откуда отсчитывается 180-дневный отрезок, стала скользящей.
                Это уже не дата дебютного въезда, а дата очередного въезда (каждого).
                При въезде, исходя из суммы дней прежних поездок за предшествующие полгода,
                определяется, сколько вы можете быть в шенгенской зоне.
                Учитываются все въезды: и по кратковременным, и по долгосрочным визам.
            </p>

            <p>После этих изменений делом первой необходимости стал подсчет: сколько еще
                у вас осталось дней для наслаждения странами Шенгенской зоны. Как это ни
                печально, но «просчитавшегося», исчерпавшего свой лимит, могут и завернуть
                на границе в обратном направлении. Чтобы этого избежать, мы предлагаем вам
                визовый калькулятор, ведь миссия подсчета возложена на туриста.
            </p>
        </div>


        <div class="iwt1-visa-title iwt1-margintop-40">
            Какие поездки не надо включать
            в общую сумму?
        </div>

        <div class="iwt1-text iwt1-text-16 iwt1-text-light">
            <p>Используя калькулятор шенген, исключайте из подсчетов визиты в Болгарию, Великобританию,
                Румынию, Хорватию и на Кипр. Эти страны — не шенгенские.
            </p>

            <p>А вот Лихтенштейн, Исландия, Норвегия и Швейцария — наоборот, и пусть вас
                не смущает факт, что они не являются членами ЕС.
            </p>

            <p>Наш калькулятор сохранит вам душевный комфорт и немаленькие суммы:</p>


            <ul class="iwt1-docs-list iwt1-visa-list">
                <li>
                    1000 евро (примерная стоимость путевки,
                    которая пропадает, если вас не впустят на границе);
                </li>
                <li>
                    1200 евро (штраф за превышение срока присутствия
                    в шенгенской зоне на длительный срок);
                </li>
                <li>
                    600 евро (штраф за кратковременное превышение срока).
                </li>
            </ul>
        </div>
    </div>
<?php get_footer();