<?php
get_header();
global $theme;
?>
<div class="container-fluid">
    <div class="iwt1-pagetitle iwt1-pagetitle-company">
        Частые вопросы
    </div>
    <a href="<?php echo $theme->sitevar('Ссылка блока цены и сроки'); ?>" class="btn iwt1-gray-btn iwt1-gray-btn-small">цены и сроки</a>
    <div class="iwt1-text">
        <p>
            <?php echo $theme->sitevar('Текст под заголовком для страницы ' . str_replace('/', (' '), (string) $_SERVER['REQUEST_URI'])); ?>
        </p>
    </div>    
    <ul class = "iwt1-faq-list">
        <?php
        $args = array('post_type' => 'faq', 'order' => 'ASC', 'orderby' => 'meta_value_num', 'posts_per_page' => 1000, 'meta_key' => 'wpcf-ordernum');
        $loop = new WP_Query($args);
        while ($loop->have_posts()) : $loop->the_post();
            ?>
            <!--серый заголовок-->
            <li>
                <a href = "#" class = "iwt1-faq-list-link clearfix">
                    <span class = "iwt1-arrow pull-left">&nbsp;
                    </span>
                    <span class = "iwt1-faq-list-link-text pull-left"><?php the_title(); ?></span>
                </a>

                <!--подменю-->
                <ul class = "iwt1-faq-sublist">
                    <?php
                    $content = do_shortcode(get_the_content());
                    echo $content;
                    ?>      
                </ul>
                <!--подменю-->
            </li>
            <!--серый заголовок-->        
        <?php endwhile; ?>
    </ul>
</div>
<?php
get_footer();
