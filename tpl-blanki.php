<?php
global $theme;
get_header();
while (have_posts()) {
    the_post();
    ?>
    <div class="container-fluid">
        <div class="iwt1-pagetitle iwt1-pagetitle-blanki">
            <?php the_title(); ?>
        </div>

        <a href="<?php echo $theme->sitevar('Ссылка блока цены и сроки'); ?>" class="btn iwt1-gray-btn iwt1-gray-btn-small">цены и сроки</a>
        <div class="iwt1-text">
            <p><?php
                if (is_single() || is_page()) {

                    $text = $theme->field('Текст под заголовком');
                } else {
                    $text = $theme->sitevar('Текст под заголовком для страницы ' . str_replace('/', (' '), (string) $_SERVER['REQUEST_URI']));
                }
                if (strlen($text)) {
                    ?>
                    <?php echo $text; ?>
                <?php }
                ?></p>
        </div>
        <?php
        $fattach = array(
            'Загранпаспорт нового образца (биометрический) для взрослых:',
            'Загранпаспорт нового образца (биометрический) для детей:',
            'Загранпаспорт старого образца для взрослых:',
            'Загранпаспорт старого образца для детей:',
                //    'Дополнительно'
        );
        $classsuffix='';
        foreach ($fattach as $fat) {
            $files = $theme->attachments($fat);
            if ($files) {
                require(locate_template('block-blanki-ul.php'));
                ?>

                <?php
            }
        } $fattach = array('Дополнительно');
        $files = $theme->attachments($fattach[0]);
        ?>

    </div>




    <!--серый блок "дополнительно"-->
    <?php
    $classsuffix = 'iwt1-blank-block-additional';
    require(locate_template('block-blanki-ul.php'));
    ?> 
<?php } ?>
<?php
get_footer();
