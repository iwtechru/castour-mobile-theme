<?php
global $theme;
get_header();
?>
<?php while (have_posts()) : the_post(); ?>
    <div class="container-fluid">

        <div class="iwt1-pagetitle iwt1-pagetitle-blanki">
            <?php the_title(); ?>
        </div>

        <a href="<?php echo $theme->sitevar('Ссылка блока цены и сроки'); ?>" class="btn iwt1-gray-btn iwt1-gray-btn-small">цены и сроки</a>

        <div class="iwt1-text iwt1-text-16 iwt1-text-light"><p>
                <?php
                if (is_single() || is_page()) {

                    $text = $theme->field('Текст под заголовком');
                } else {
                    $text = $theme->sitevar('Текст под заголовком для страницы ' . str_replace('/', (' '), (string) $_SERVER['REQUEST_URI']));
                }
                if (strlen($text)) {
                    ?>  
                    <?php
                    echo $text;
                }
                ?>
            </p>
        </div>



        <ul class = "iwt1-blanki-list iwt1-blanki-list-dotted">
            <!--ВНИМАНИЕ!по умолчанию иконка у li в виде листа, но если нужно для квитанции, то
            добавить класс iwt1-kvit-icon-->
            <?php
            $files = $theme->attachments('Документы');
            foreach ($files as $file) {
                ?><li><a href="<?php echo wp_get_attachment_url($file['id']); ?>" class="iwt1-orange-link"><?php
                        echo $file['fields']['title'];
                        ?></a></li><?php } ?>
        </ul>
    </div>
<?php endwhile;
?>
<?php
get_footer();
