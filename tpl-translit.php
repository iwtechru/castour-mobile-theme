<?php get_header();
while (have_posts()) {
    the_post();
?><div class="container-fluid">
    <div class="iwt1-pagetitle iwt1-pagetitle-translate">
        <?php the_title();?>
    </div>

    <a href="<?php echo $theme->sitevar('Ссылка блока цены и сроки'); ?>" class="btn iwt1-gray-btn iwt1-gray-btn-small">цены и сроки</a>

    <div class="iwt1-translit-text"><?php
            $content = explode('<hr />', get_the_content());
            echo $content[0];
            ?></div>

    <div class="clearfix iwt1-traslate-wrapper">
        <input type="text" name="translitLastname" 
               class="iwt1-translit-input-mobile margin-bottom-14" 
               id="iwt1-translit-lastname" placeholder="Фамилия"/>
        <input type="text" name="translitName" 
               class="iwt1-translit-input-mobile margin-bottom-14" 
               id="iwt1-translit-name" placeholder="Имя" />

        <button type="button" class="btn iwt1-orange-btn-block"
                id="js-translit">
            Транслитерировать
        </button>
    </div>


    <div class="iwt1-translit-wrapper">
        <div class="iwt1-translit-passport iwt1-translit-passport-mobile">
            <div class="iwt1-lastname-wrapper">
                <div class="iwt1-lastname-rus" id="iwt1-lastname-rus">Фамилия</div>    
                <div class="iwt1-lastname-translit" id="iwt1-lastname-translit">Familia</div>    
            </div>
            <div class="iwt1-name-wrapper">
                <div class="iwt1-name-rus" id="iwt1-name-rus">Имя</div>    
                <div class="iwt1-name-translit" id="iwt1-name-translit">Imia</div>    
            </div>
            <div class="iwt1-passpost-bottom" id="iwt1-passpost-bottom">р&lt;rusFamiliia&lt;&lt;Imia&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;
                <br />XXXXXXXXXXRUSXXXXXXXXXXXXXXXXXXXXXXXX&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;15
            </div> 
        </div>
        <div class="iwt1-translit-rules iwt1-translit-rules-mobile hidden">
            <img src="<?php bloginfo('template_url');?>/css/img/rules-mobile.png" class="img-responsive" alt=""/>
            <a href="#" class="iwt1-close-rules"></a>
        </div>
    </div>
    <button type="button" class="iwt1-gray-btn iwt1-show-rules-mobile" 
            id="iwt1-show-rules">
        Новые правила<wbr> транслитерации
    </button>
</div>
<?php

}
get_footer();
