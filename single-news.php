<?php
get_header();
global $theme;
while (have_posts()) {
    the_post();
    ?>
    <div class="container-fluid">
        <div class="iwt1-pagetitle iwt1-pagetitle-news">
            <?php the_title(); ?>
        </div>
        <?php if (get_post_type() == 'news'||get_post_type() == 'post') { ?>
            <div class="iwt1-ads-date iwt1-ads-date-mobile"><?php echo $theme->get_the_time('%d %FS %Y'); ?></div>
        <?php } ?>
        <?php get_template_part('block', 'extratext'); ?>
        <?php
        $img = $theme->get_thumb_src();
        if (strlen($img)) {
            ?>
            <div class="iwt1-image-wrap">
                <img src="<?php echo $img; ?>" alt="<?php the_title(); ?>" class="img-responsive" />
            </div>
        <?php } ?>
        <div class="iwt1-text iwt1-text-light iwt1-text-16">
            <?php the_content(); ?>
        </div>

        <?php
        if (get_post_type() == 'post') {
            $link = get_category_link(31);
        } else {
            $link = get_post_type_archive_link(get_post_type());
        }
        ?>
        <a href="<?php echo $link; ?>" class="btn iwt1-gray-btn iwt1-back-to-ads">вернуться к списку<wbr> <?php
            switch (get_post_type()) {
                case 'article':
                    echo 'СТАТЕЙ';
                    break;
                case 'news':
                    echo 'НОВОСТЕЙ';
                    break;
            }
            ?></a>
    </div>
    <?php
}
get_footer();
