<?php
get_header();
while (have_posts()) {
    the_post();
    ?>
    <div class="container-fluid padding-bottom-30">
        <div class="iwt1-pagetitle iwt1-pagetitle-about">
           <?php the_title();?>
        </div>
        <a href="<?php echo $theme->sitevar('Ссылка блока цены и сроки'); ?>" class="btn iwt1-gray-btn iwt1-gray-btn-small">цены и сроки</a>


        <div class="iwt1-text iwt1-text-16 iwt1-text-light iwt1-text-addresspage">
          <?php the_content(); ?>
        </div>


    </div>
    <?php
}
get_footer();
