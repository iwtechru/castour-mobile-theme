<?php
get_header();
while (have_posts()) {
    the_post();
    ?>
    <div class="container-fluid">
        <div class="iwt1-pagetitle iwt1-pagetitle-zagran">
            <?php the_title(); ?>
        </div>
        <a href="<?php echo $theme->sitevar('Ссылка блока цены и сроки'); ?>" class="btn iwt1-gray-btn iwt1-gray-btn-small">цены и сроки</a>
        <div class="iwt1-zagran-wrapper">
            <img src="<?php echo $theme->get_thumb_src(get_the_ID(), 586,391, true); ?>" alt="" class="img-responsive" />
            <div class="iwt1-zargan-orange">
                <?php echo $theme->field('Текст рыжего блока', array('notsingle' => true, 'variant' => 'textarea')); ?>        </div>
        </div>
        <div class="iwt-content iwt1-text iwt1-text-16 iwt1-text-light"><?php the_content(); ?></div>
 

    </div>
    <?php
} get_footer();
