<?php
/*
  Template name: Главная страница
 */
  get_header();

  while (have_posts()) {
    the_post();


    $content = explode('<hr />', get_the_content());
    ?>
    <!--оранжевый блок с паспортом по центру-->
    <div class="clearfix iwt1-orange-block">
        <div class="iwt1-title">
            Помощь в оформлении загранпаспорта
        </div>
        <div class="iwt1-white-border">
            <div class="iwt1-orange-title clearfix">
                <div class="iwt1-orange-icon iwt1-orange-icon-check pull-left"></div>
                <div class="pull-left">
                    <div class="iwt1-check-title"><a href="#auth" data-target="#auth" data-toggle="modal" class="">Проверить готовность</a></div>
                    <div class="iwt1-clients-only">(Только для наших клиентов)</div>
                </div>
            </div>
        </div>
        <?php 
        $items=array('about'=>'О нас','services'=>"Услуги",'help'=>'Справочная','userful'=>'Полезное');
        foreach($items as $key=>$value){?>
        <div class="iwt1-white-border">
            <a role="button" class="iwt1-orange-title clearfix" 
            data-toggle="collapse" 
            href="#submenu<?php echo $key;?>" 
            aria-expanded="true" 
            aria-controls="submenu<?php echo $key;?>"
            >
            <span class="iwt1-orange-icon iwt1-orange-icon-<?php echo $key;?> pull-left"></span>
            <span class="pull-left">
               <?php echo $value;?>
           </span>
           <span class="iwt1-arrow pull-right"></span>            
       </a>

       <!--подменю-->
       <ul class="panel-collapse collapse iwt1-list iwt1-submenu-index" id="submenu<?php echo $key;?>">
        <?php foreach ($theme->menu_items($value.' на главной (мобильная версия)') as $item) { ?>
        <li><a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
            <?php } ?>
        </ul>
    </div>
    <?php }
    if(2==1){?>
    <div class="iwt1-white-border">
        <div class="iwt1-orange-title clearfix">
            <div class="iwt1-orange-icon iwt1-orange-icon-about pull-left"></div>
            <div class="pull-left">
                <div>О нас</div>
            </div>
            <span class="iwt1-arrow pull-right"></span>
        </div>
        <ul class="iwt1-list">
            <?php foreach ($theme->menu_items('О нас на главной') as $item) { ?>
            <li><a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
                <?php } ?>
            </ul>
        </div>


        <div class="iwt1-white-border">
            <div class="iwt1-orange-title clearfix">
                <div class="iwt1-orange-icon iwt1-orange-icon-help pull-left"></div>
                <div class="pull-left">
                    <div>Справочная</div>
                </div>
                <span class="iwt1-arrow pull-right"></span>
            </div>
            <ul class="iwt1-list">
                <?php foreach ($theme->menu_items('Услуги на главной') as $item) { ?>
                <li><a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
                    <?php } ?>
                </ul>
            </div>


            <div class="iwt1-white-border">
                <div class="iwt1-orange-title clearfix">
                    <div class="iwt1-orange-icon iwt1-orange-icon-services pull-left"></div>
                    <div class="pull-left">
                        <div>Услуги</div>
                    </div>
                    <span class="iwt1-arrow pull-right"></span>
                </div>
                <ul class="iwt1-list">
                    <?php foreach ($theme->menu_items('Справочная на главной') as $item) { ?>
                    <li><a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
                        <?php } ?>
                    </ul>
                </div>

                <div class="iwt1-white-border">
                    <div class="iwt1-orange-title clearfix">
                        <div class="iwt1-orange-icon iwt1-orange-icon-userful pull-left"></div>
                        <div class="pull-left">
                            Полезное
                        </div>
                        <span class="iwt1-arrow pull-right"></span>
                    </div>
                    <ul class="iwt1-list">
                        <?php foreach ($theme->menu_items('Полезное') as $item) { ?>
                        <li><a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php } ?>
                </div>


                <!--белый блок с текстом и оранжевыми кружочками-->
                <div class="iwt1-white-block">
                    <?php echo $content[0]; ?>
                </div>
                <div class="iwt1-days-block">
                    <div class="iwt1-days-bold">
                        <div class="container-fluid">
                            <?php echo $theme->field('Текст над тремя кружками', array('notsingle' => false)); ?>
                        </div>
                    </div>
                    <div class="clearfix">
                        <?php
                        for ($i = 1; $i <= 3; $i++) {
                            $days = explode(' ', $theme->field('Текст в кружке №' . $i, array('notsingle' => false)));
                            ?>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="iwt1-day">
                                    <div class="iwt1-day-number"><?php echo $days[0]; ?></div>
                                    <div class="iwt1-day-text"><?php echo $days[1]; ?></div>
                                </div> 
                                <div class="iwt1-day-title"><?php echo preg_replace('/^([^\ ]*)\ /', '$1<br/>', $theme->field('Текст под кружком №' . $i, array('notsingle' => false))); ?></div>
                            </div>    
                            <?php } ?>
                        </div>
                    </div>
                    <div class="iwt1-white-block iwt1-white-block-bottom">

                        <?php echo $content[1]; ?>
                    </div>
                    <!--белый блок с текстом и оранжевыми кружочками-->


                    <!--блок три характеристики-->
                    <div class="clearfix iwt1-gray-block">
                        <!--<div class="wrap">-->
                        <div class="clearfix iwt1-title-bold">
                            <?php
                            echo $theme->field('Заголовок над характеристиками', array('notsingle' => false, 'default' => '<b>3 характеристики</b> <br />
                                наших услуг'));
                                ?>
                            </div>

                            <div class="container-fluid">
                                <div class="row">
                                    <?php for ($i = 1; $i <= 3; $i++) { ?>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="iwt1-three-icon iwt1-three-icon-<?php echo $i; ?>"></div>
                                        <div class="iwt1-three-uppercase">
                                            <?php
                                            $field_label = 'Характеристика №' . $i . ' (используйте || для большого отступа)';
                                            $args = array('notsingle' => false);
                                            $field_value = $theme->field($field_label, $args);
                                            $plus = explode('||', $field_value);
                                            ?>
                                            <?php echo $plus[0]; ?>
                                        </div>
                                        <div class="iwt1-col-text">
                                            <?php echo $plus[1]; ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <!--блок три характеристики-->


                        <!--блок 5 обещаний-->
                        <div class="iwt1-five-block">
                            <div class="container-fluid">
                                <div class="iwt1-title-bold">
                                    <?php echo $theme->field('Заголовок над кругами с процентами', array('notsingle' => false)); ?>
                                </div>
                                <div class="row">
                                    <div class="clearfix">
                                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                                        <div class="iwt1-col-five">
                                            <div class="iwt1-five-icon"></div>
                                            <div class="iwt1-col-text iwt1-col-text-20">
                                                <?php echo $theme->field('Текст под кругом №' . $i, array('notsingle' => false)) ?>
                                            </div>
                                        </div>           
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--блок 5 обещаний-->

                        <!--блок 7 преимуществ-->
                        <div class="clearfix iwt1-seven-block">
                            <div class="wrap">
                                <div class="clearfix iwt1-title-bold">
                                    <?php echo $theme->field('Заголовок блока преимуществ', array('notsingle' => false)); ?>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="clearfix">
                                            <?php for ($i = 1; $i <= 4; $i++) { ?>
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                <div class="iwt1-seven-icon iwt1-seven-icon-<?php echo $i; ?>"></div>
                                                <div class="iwt1-col-text iwt1-col-text-20">
                                                    <?php echo $theme->field('Преимущество №' . $i, array('notsingle' => false)); ?>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <div class="iwt1-small-row">
                                            <?php for ($i = 5; $i <= 7; $i++) { ?>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <div class="iwt1-seven-icon iwt1-seven-icon-<?php echo $i; ?>"></div>
                                                <div class="iwt1-col-text iwt1-col-text-20">
                                                    <?php echo $theme->field('Преимущество №' . $i, array('notsingle' => false)); ?>
                                                </div>
                                            </div>
                                            <?php } ?>                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--блок 7 преимуществ-->


                        <!--блок с объявлениями-->
                        <div class="iwt1-adv-wrapper">
                            <div class="iwt1-title-bold">
                                <?php
                                $field = $theme->field('Текст над объявлениями', array('notsingle' => true));
                                if (strlen($field) > 0) {
                                    echo $field;
                                } else {
                                    ?><b>Берём на себя ваши заботы</b> <br />
                                    нашей компании<?php } ?>
                                </div>
                                <div class="iwt1-adv-text">
                                    Вы пребываете в зоне комфорта, пока мы сохраняем ваше<br /> время и нервную систему.
                                </div>
                                <div class="iwt1-title-bold">
                                    <b>Объявления</b>
                                </div>
                                <div class="container-fluid iwt1-advs">
                                    <div class="clearfix">
                                        <?php
                                        $news = get_posts(array('post_type' => 'post', 'posts_per_page' => 3));
                                        foreach ($news as $post) {
                                            setup_postdata($post);
                                            ?>
                                            <div class="col-md-4 col-sm-12 col-xs-12 iwt1-adv-col">
                                                <a href="<?php echo post_permalink();?>" class="iwt1-adv-link"><?php the_title(); ?></a>
                                                <div class="iwt1-adv-date"><?php echo $theme->get_the_time('%d/%m/%Y');?></div>
                                                <?php echo preg_replace('/^([^$]{300}[^\ ]*)\ [^$]*$/', '$1...', get_the_excerpt()); ?>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <a href="<?php echo get_category_link(31); ?>" class="btn iwt1-gray-btn">Все объявления</a>
                                    </div>
                                </div>
                                <!--блок с объявлениями--><?php
                            }get_footer();

