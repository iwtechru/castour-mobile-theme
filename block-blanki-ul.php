<div class="iwt1-blank-block <?php echo $classsuffix; ?>">
    <div class="iwt1-blank-subtitle">
        <?php echo $fat; ?>
    </div>
    <ul class="iwt1-blanki-list">
        <!--            ВНИМАНИЕ! по умолчанию иконка у li в виде листа, но если нужно для квитанции, то
                    добавить класс iwt1-kvit-icon-->
        <?php foreach ($files as $file) { ?>    
            <?php
            $san = $theme->sanitize($file['fields']['title']);
            $sant = explode('-', $san);
            $sant1 = preg_replace('/\d/', '', $sant[0]);
            ?>
            <li <?php if ($sant1 == 'kvitantsi') { ?>class="iwt1-kvit-icon"<?php } ?>>
                <a href="<?php echo $theme->get_thumb_src($file['id']); ?>" class="iwt1-orange-link"> 
                    <?php echo $file['fields']['title']; ?>
                </a>
            </li>
        <?php } ?>
    </ul>
</div>