<?php
get_header();
global $theme;
get_template_part('block', 'extratext');
?>
<?php query_posts('cat=31&showposts=999');
?>
<div class="container-fluid">
    <div class="iwt1-pagetitle iwt1-pagetitle-ads">
        <?php the_title(); ?>
    </div>
</div>
<?php
$countposts = 0;
while (have_posts()) {
    the_post();
    ?>
    <div class="iwt1-ads-col-mobile">
        <a href="<?php echo post_permalink();?>" class="iwt1-ads-link iwt1-ads-link-mobile"><?php the_title(); ?></a>
        <div class="iwt1-ads-date iwt1-ads-date-mobile"><?php echo $theme->get_the_time('%d %FS %Y'); ?></div>
        <div class="iwt1-ads-text iwt1-ads-text-mobile"><?php echo preg_replace('/^([^$]{300}[^\ ]*)\ [^$]*$/', '$1...', get_the_excerpt()); ?></div>
    </div>
<?php } ?>
<?php
get_footer();
