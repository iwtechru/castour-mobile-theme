<?php
get_header();
while (have_posts()) {
    the_post();
    ?><div class="container-fluid">
        <div class="iwt1-pagetitle iwt1-pagetitle-about">
            <?php the_title(); ?>
        </div>
        <div class="row">
            <div class="iwt1-about-orange">
                <div class="iwt1-about-title">
                    <?php echo $theme->field("Заголовок рыжего блока"); ?>
                </div>
                <div class="iwt1-about-italic">
                    <?php echo $theme->field('Текст рыжего блока'); ?>
                </div>
            </div>
        </div>
        <div class="iwt1-text iwt1-text-16 iwt1-text-light">
            <p><?php            
            $content = explode('<hr />', get_the_content());
            echo $content[0];
            ?></p>
        </div>

        <?php
        $files = $theme->attachments('Кто о нас пишет');
        foreach ($files as $file) {
            ?>
            <div class="iwt1-about-col iwt1-text iwt1-text-16 iwt1-text-light" >
                <a href="<?php echo $file['fields']['title']; ?>" class="iwt1-about-icons iwt1-about-icon-1" target="_blank"  style="background-image:url(<?php echo $theme->get_thumb_src($file['id'],'original'); ?>);"></a>
                <p>
                    <?php echo $file['fields']['caption']; ?>
                </p>
                <a href="<?php echo $file['fields']['title']; ?>" class="iwt1-about-readmore">Подробнее</a>
            </div>
        <?php } ?>

        <div class="iwt1-text iwt1-text-16 iwt1-text-light">
            <p><?php echo $content[1]; ?></p>
        </div>



    </div><?php
}
get_footer();
