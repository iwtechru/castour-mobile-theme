<h1>Настройки темы</h1>
<form method="post" action="options.php">
	<?php
	settings_fields( $this->prefix . '-settings-group' );
	$sitevars = ($this->get_option( 'sitevars' ));
	ksort($sitevars);
	foreach ( $sitevars as $key => $value ) {
		$params = array();
		$params['name'] = $key;
		$field_value = get_option( $key );
		if ( !$field_value ) {
			$field_value = $value['default'];
		}
		$params['field_name'] = $key;
		$params['field_value'] = $field_value;
		$params['value'] = array(
			'label' => $value['name'],
		);
		if ( $value['admin'] == 1 && !is_admin() ) {
			break;
		}
		$this->include_block( 'cf/' . $value['variant'], $params );
		?>
	        <!-- Короткий код для проставления в текстах ("шорткод"): <strong>[<?php echo $value['name']; ?>]</strong><br/> --><?php }
	?>    
	<?php
	submit_button();
	?>
</form>