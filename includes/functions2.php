<?php

/**
 * Created by PhpStorm.
 * User: Oleg0s
 * Date: 22.02.15
 * Time: 13:26
 */
function ajax_login_init() {
    wp_register_script('ajax-login-script', get_template_directory_uri() . '/assets/js/ajax-login-script.js', array('jquery'));
    wp_enqueue_script('ajax-login-script');

    wp_localize_script('ajax-login-script', 'ajax_login_object', array(
        'ajaxurl' => admin_url('admin-ajax.php'),
        'redirecturl' => '/lk/',
        'loadingmessage' => 'Проверка...'
    ));

    add_action('wp_ajax_nopriv_ajaxlogin', 'ajax_login');
}

if (!is_user_logged_in()) {
    add_action('init', 'ajax_login_init');
}

function ajax_login() {

    check_ajax_referer('ajax-login-nonce', 'security');

    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;

    $user_signon = wp_signon($info, false);
    if (is_wp_error($user_signon)) {
        echo json_encode(array('loggedin' => false, 'message' => 'Неправильный договор или пароль.'));
    } else {
        echo json_encode(array('loggedin' => true, 'message' => 'Вход...'));
    }

    die();
}

/* ------------------------------------------------------------------------------------------------------------- */

/**
 * redirect Users to dogovor page
 */
//add_filter('login_redirect', 'dogovor_login_redirect', 10, 3 );

function dogovor_login_redirect($url, $request, $user) {
    if ($user && is_object($user) && is_a($user, 'WP_User')) {
        $url = home_url('/lk/');
        if ($user->has_cap('administrator')) {
            $url = admin_url();
        }
        if ($user->has_cap('editor') || $user->has_cap('manage_dogovors')) {
            $url = admin_url('edit.php?post_type=dogovor');
        }
    }
    return $url;
}

add_filter('wp_insert_post_data', 'modify_dogovor_post', '99', 2);

function modify_dogovor_post($data, $postarr) {
    $num = $postarr['wpcf']['dogovor_num'];
    if (isset($num)) {
        if ($data['post_type'] == 'dogovor') {
            $data['post_title'] = 'Договор № ' . $num;
            $data['post_name'] = $num;
        }
        $pass = check_user($num, $postarr['wpcf']['dogovor_name'], $postarr['wpcf']['dogovor_surname']);
        if ($pass) {
            if (!update_post_meta($_POST['post_ID'], 'dogovor_password', $pass))
                add_post_meta($_POST['post_ID'], 'dogovor_password', $pass, true);;
            $message = 'Ваше имя пользователя:' . $num . "\r\nПароль:" . $pass . "\r\n" . home_url();
            $email = $postarr['wpcf']['dogovor_e_mail'];
            wp_mail($email, 'castour Личный кабинет', $message);
            $message = "Зарегистрирован новый договор\r\nИмя пользователя:" . $num . "\r\nПароль:" . $pass;
            global $user_email;
            get_currentuserinfo();
            wp_mail($user_email, 'castour Новый договор', $message);
        }
        if (isset($_POST['new_password']) && $_POST['new_password'] != '') {
            $pass = $_POST['new_password'];
            $user = get_user_by('login', $num);
            if ($user) {
                wp_set_password($pass, $user->id);
                if (!update_post_meta($_POST['post_ID'], 'dogovor_password', $pass))
                    add_post_meta($_POST['post_ID'], 'dogovor_password', $pass, true);;
                global $theme;
                $theme->dash_notice[] = 'Пароль для договора изменен.';
                $message = 'Ваше имя пользователя:' . $num . "\r\nПароль:" . $pass . "\r\n" . home_url();
                $email = $postarr['wpcf']['dogovor_e_mail'];
                wp_mail($email, 'castour Пароль для договора изменен', $message);
                $message = "Пароль для договора изменен\r\nИмя пользователя:" . $num . "\r\nПароль:" . $pass;
                global $user_email;
                get_currentuserinfo();
                wp_mail($user_email, 'castour Пароль для договора ' . $num . ' изменен', $message);
            }
        }
    }
    return $data;
}

function check_user($user_name, $first_name, $sur_name) {
    $password = wp_generate_password();
    $user_data = array(
        'ID' => '',
        'user_pass' => $password,
        'user_login' => $user_name,
        'user_nicename' => $user_name,
        'user_url' => '',
        'user_email' => $user_name . '@dummy.com',
        'display_name' => $user_name,
        'nickname' => $user_name,
        'first_name' => $first_name,
        'last_name' => $sur_name,
        'role' => get_option('default_role')
    );
    $user_id = wp_insert_user($user_data);
    if (is_wp_error($user_id)) {
        return false;
    }
    return $password;
}

add_action('init', 'iwto_remove_admin_menu');

function iwto_remove_admin_menu() {
    if (!current_user_can('administrator') && !current_user_can('editor') && !current_user_can('manage_dogovors')):
        show_admin_bar(false);
    endif;
}

/* ------------------------------------------------------------------------- */

function get_dogovor_state($date_from, $date_to, $real_final) {
    date_default_timezone_set('Europe/Moscow');
    global $wdays;
    if(!$wdays){
        die('Ой');
    }
    $days_relations = array();
    for ($i = 23; $i >= 1; $i--) {
        $minus_working_days_ready = 0;
        $minus_working_days = 5;
        if ($i < 20) {
            $minus_working_days = 4;
            $minus_working_days_ready = 1;
        }
        if ($i < 16) {
            $minus_working_days = 3;
            $minus_working_days_ready = 2;
        }
        if ($i < 10) {
            $minus_working_days = 2;
            $minus_working_days_ready = 3;
        }
        if ($i < 6) {
            $minus_working_days = 1;
            $minus_working_days_ready = 4;
        }
        $days_relations[$i]['wdays_check'] = $i - $minus_working_days;
        $days_relations[$i]['wdays_ready'] = $i + $minus_working_days;
    }
    $current_date = time() - 60 * 60;
    //$current_date = mktime(0, 0, 0, 1, 29, 2015);
    $passport_ready_working_days = $wdays->countworking_days($date_from, $real_final);
    $prwd = $passport_ready_working_days;
    $docs_submitted = $wdays->rewind_unix_time($date_from) + 60 * 60 * 19;
    $docs_are_being_checked = $wdays->add_working_days($date_from, 1);
    $docs_are_checked = $wdays->add_working_days($docs_submitted, $days_relations[$prwd]['wdays_check']) + 60 * 60 * 10;
    $passport_readiness = $docs_are_checked + 1;
    $passport_ready = $wdays->rewind_unix_time($real_final) + 60 * 60 * 10;
    if ($_GET['debug'] == 1) {
        ?>
        <table>
            <tr>
                <td>Сейчас:</td><td><?php echo date('d/m/Y H:i:s', $current_date); ?></td>
            </tr> 
            <tr>
                <td>Количество рабочих дней на изготовление паспорта:</td><td><?php echo ( $passport_ready_working_days); ?></td>
            </tr>
            <tr>
                <td>Документы поданы:</td><td><?php echo date('d/m/Y H:i:s', $docs_submitted); ?></td>
            </tr>
            <tr>
                <td>Проверка поданных документов:</td><td><?php echo date('d/m/Y H:i:s', $docs_are_being_checked); ?>
                    <br/>(<?php echo $wdays->countworking_days($date_from, $docs_are_being_checked); ?> раб.дн.)</td>
            </tr>         
            <tr>
                <td>Проверки пройдены:</td><td><?php echo date('d/m/Y H:i:s', $docs_are_checked); ?></td>
            </tr> 
            <tr>
                <td>Готовность паспорта:</td><td><?php echo date('d/m/Y H:i:s', $passport_readiness); ?></td>
            </tr> 
            <tr>
                <td>Паспорт готов:</td><td><?php echo date('d/m/Y H:i:s', $passport_ready); ?></td>
            </tr> 
        </table>
        <?php
    }
    if ($current_date >= $passport_ready) {
        return 5; //Паспорт готов            
    }
    if ($current_date >= $passport_readiness) {
        return 4; // Готовность паспорта
    }
    if ($current_date >= $docs_are_checked) {
        return 3; // Проверки пройдены
    }
    if ($current_date >= $docs_are_being_checked) {
        return 2; // Проверка поданных документов
    }
    if ($current_date >= $docs_submitted) {
        return 1; // Документы поданы
    }
    return 0;
}

//add_action( 'iwto_passport_ready_event','iwto_send_passport_ready_notification');
//wp_schedule_single_event(time(),'iwto_passport_ready_event',array('frag99@gmail.com') );
add_action('iwto_passport_ready_event', 'iwto_send_passport_ready_notification');

function iwto_send_passport_ready_notification($email) {
    $message = "Ваш паспорт готов.\nЗабор паспорта – в день готовности после 15:00, в последующие дни в любое время.";
    wp_mail($email, 'Ваш паспорт готов.', $message);
}

add_action('admin_init', 'iwto_add_manager_role');

function iwto_add_manager_role() {
    global $theme;
    $role = get_role('dogovor_manager');
    if (!isset($role)) {
        $result = add_role(
                'dogovor_manager', 'Менеджер договоров', array(
            'read' => true,
            'delete_posts' => false,
            'manage_dogovors' => true,
            'edit_dogovor' => true,
            'read_dogovor' => true,
            'delete_dogovor' => true,
            'edit_others_dogovor' => true,
            'publish_dogovor' => true,
                )
        );
        if (null !== $result) {
            $theme->dash_notice[] = 'Роль менеджера договоров создана!';
        }
    } else {
        iwto_add_manager_caps($role);
    }
    $role = get_role('administrator');
    iwto_add_manager_caps($role);
    $role = get_role('editor');
    iwto_add_manager_caps($role);
    /*    $dog_type = get_post_type_object('dogovor');
      if(isset($dog_type)){
      //dbg($dog_type);
      } */
}

function iwto_add_manager_caps($role) {
    $role->add_cap('manage_dogovors');
    $role->add_cap('edit_dogovor');
    $role->add_cap('read_dogovor');
    $role->add_cap('delete_dogovor');
    $role->add_cap('edit_others_dogovor');
    $role->add_cap('publish_dogovor');
    $role->add_cap('read_private_dogovor');
    $role->add_cap('delete_private_dogovor');
    $role->add_cap('delete_published_dogovor');
    $role->add_cap('delete_others_dogovor');
    $role->add_cap('edit_private_dogovor');
    $role->add_cap('edit_published_dogovor');
}

add_action('registered_post_type', 'iwto_change_custom_post_types', 10, 2);

function iwto_change_custom_post_types($post_type, $args) {
    if (in_array($post_type, array('dogovor', 'dogovor_passport', 'dogovor_sms', 'sms', 'fms_address', 'dogovor_fms_address'))) {
        $args->capability_type = 'dogovor';
        $args->cap->edit_post = 'edit_dogovor';
        // $args->cap->edit_post='edit_sms';
        $args->cap->read_post = 'read_dogovor';
        $args->cap->delete_post = 'delete_dogovor';
        $args->cap->edit_posts = 'edit_dogovor';
        $args->cap->edit_others_posts = 'edit_others_dogovor';
        $args->cap->publish_posts = 'publish_dogovor';
        $args->cap->read_private_posts = 'read_private_dogovor';
        $args->cap->delete_posts = 'delete_dogovor';
        $args->cap->delete_private_posts = 'delete_private_dogovor';
        $args->cap->delete_published_posts = 'delete_published_dogovor';
        $args->cap->delete_others_posts = 'delete_others_dogovor';
        $args->cap->edit_private_posts = 'edit_private_dogovor';
        $args->cap->edit_published_posts = 'edit_published_dogovor';
        $args->cap->create_posts = 'edit_dogovor';
        //$args->cap->create_posts='edit_sms';
        $wp_post_types[$post_type] = $args;
    }
}

add_action('admin_menu', 'iwto_remove_menu_items');

function iwto_remove_menu_items() {
    if (!current_user_can('edit_posts')) {
        $menus = $GLOBALS['menu'];
        foreach ($menus as $menu) {
            if ($menu[2] != 'edit.php?post_type=dogovor' && $menu[2] != 'edit.php?post_type=fms_address' && $menu[2] != 'edit.php?post_type=sms') {
                remove_menu_page($menu[2]);
            }
        }
    }
}

add_action('init', 'iwto_prevent_profile_access');

function iwto_prevent_profile_access() {
    if (current_user_can('edit_posts'))
        return;
    if (strpos($_SERVER ['REQUEST_URI'], 'wp-admin/profile.php')) {
        if (current_user_can('edit_dogovor')) {
            wp_redirect(admin_url('edit.php?post_type=dogovor'));
        } else {
            wp_redirect(home_url('/lk/'));
        }
    }
}

/* ------------------------------------------------------------------------ */

add_action('add_meta_boxes_dogovor', 'iwto_add_password_meta_box');

function iwto_add_password_meta_box() {
    add_meta_box(
            'password_section', 'Пароль для договора', 'iwto_password_meta_box_callback', 'dogovor', 'side'
    );
}

function iwto_password_meta_box_callback($post) {
    $pass = get_post_meta($post->ID, 'dogovor_password', true);
    if (!isset($pass))
        return;
    ?>
    <p>Пароль: <?php echo $pass ?></p>
    <p>
        <label for="new_password">Новый пароль</label>
        <input type="text" name="new_password" id="new_password">
    </p>
    <p><input type="submit" value="Изменить пароль"></p>

    <?php
}

add_action('save_post', 'iwt_dogovor_save');

function iwt_dogovor_save($post_id) {
    if (get_post_type($post_id) == 'dogovor') {
        $fields = get_post_custom($post_id);
        $mail = $fields['wpcf-dogovor_e_mail'][0];
        $child_posts = types_child_posts('dogovor_passport', $post_id);
        $send = ($_POST['wpcf']['dogovor_is_send_email']);

        foreach ($child_posts as $passport) {
            if ($send == 1) {
                if (time() < $passport->fields['dogovor_passport_date3']) {
                    (wp_schedule_single_event($passport->fields['dogovor_passport_date3'], 'iwto_passport_ready_event', array($mail)));
                    $notification_shedule = wp_next_scheduled('iwto_passport_ready_event', array($mail));
                }
            } else {
                //update_post_meta($post_id, 'dogovor_is_send_email', 2);
                $notification_shedule = wp_next_scheduled('iwto_passport_ready_event', array($mail));
                wp_unschedule_event($notification_shedule, 'iwto_passport_ready_event', array($mail));
            }
        }
    }
}

/* ------------------------------------------------------------------------- */

function dbg($obj) {
    echo('<pre>');
    if (!isset($obj)) {
        echo('Not set');
    } else {
//        var_dump($obj);
        print_r($obj);
    }
    echo('</pre>');
}
