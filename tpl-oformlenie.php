<?php
get_header();
global $theme;
?>
<?php
while (have_posts()) {
    the_post();
    ?>
    <div class="container-fluid">
        <div class="iwt1-pagetitle iwt1-pagetitle-form">
            <?php the_title(); ?>
        </div>

        <?php
        $filelink = $theme->field('Текст со ссылкой под заголовком');
        if (strlen($filelink) > 0) {
            $filelink_ = explode('|', $filelink);
            ?>
            <a class="iwt1-orange-link iwt1-doc-icon" href="<?php echo $filelink_[1]; ?>"><?php echo $filelink_[0]; ?></a>
            <?php
        } else {
            $files = $theme->attachments('Прикрепленные файлы под заголовком');
            if (is_array($files)) {
                ?>
                <?php foreach ($files as $file) { ?>
                    <a href="<?php echo $theme->get_thumb_src($file['id']); ?>" class="iwt1-orange-link iwt1-doc-icon"><?php echo $file['fields']['title']; ?></a><br/>
                <?php } ?>     
                <?php
            }
        }
        ?>
        <div class="iwt1-text iwt1-text-light iwt1-text-16 iwt1-text-form">
            <?php
            $content = explode('<hr />', get_the_content());
            echo $content[0];
            ?>
        </div>
    </div>



    <?php
    $controls = array(
        'Тип загранпаспорта' => array('Биометрический', 'Старого образца'),
        'Возраст получателя' => array('до 14 лет', 'от 14 до 17 лет', '18 лет и старше'),
        'Место регистрации' => array('Москва', "Московская область", 'Другие регионы РФ')
    );
    $variants_ = iwt_cartesian($controls);
    foreach ($variants_ as $k => $v) {
        $string = '';
        $class = 'iwt_variant';
        $id = 'iwt_variant';
        foreach ($v as $v1) {
            $string.=$v1 . ' ';
            $class.=' iwt_variant' . $theme->sanitize($v1);
            $id.='_' . $theme->sanitize($v1);
        }
        $variant[] = array('value' => $string, 'class' => $class, 'id' => $id);
    }
    iwt_passport_set_variants($variant);
    ?>  




    <div class="iwt1-yellow-block">
        <div class="iwt1-yellow-block-title">
            Расчет стоимости загранпаспорта
        </div>
        <?php
        foreach ($controls as $title => $values) {
            $i = 0;
            foreach ($values as $k => $v) {
                ?>
                <div class="panel-collapse collapse iwt1-orange-hidden <?php if ($i == 0) { ?> in <?php } ?>" id="<?php echo str_replace('obrazca', 'obraztsa', $theme->sanitize($v)); ?>" data-variant="<?php echo str_replace('obrazca', 'obraztsa', $theme->sanitize($v)); ?>"></div>
                <?php
                $i++;
            }
        }
        ?>
        <div class="iwt1-orange-form">
            <?php
            $color = array(1 => 'green', 2 => 'lightgreen', 3 => 'yellow', 4 => 'lightorange', 5 => 'orange', 6 => 'red'
            );
            foreach ($controls as $title => $values) {
                ?>
                <div class="iwt1-orange-form-label"><?php echo $title; ?>:</div>
                <div class="iwt1-passport-row">
                    <?php foreach ($values as $k => $v) { ?>
                        <a class="iwt1-orange-form-btns <?php if ($k == 0) { ?>   iwt1-orange-form-btn-active collapsed <?php } ?> iwt1-passport-type"  
                           data-variant="<?php echo str_replace('obrazca', 'obraztsa', $theme->sanitize($v)); ?>" href="#<?php echo str_replace('obrazca', 'obraztsa', $theme->sanitize($v)); ?>"
                           data-toggle="collapse" 
                           aria-expanded="true" 
                           aria-controls="<?php echo str_replace('obrazca', 'obraztsa', $theme->sanitize($v)); ?>"
                           ><?php echo $v; ?></a>
                       <?php } ?>
                </div>    
            <?php } ?>
        </div>


        <div class="iwt1-price-form">
            <div class="iwt1-price-form-title">
                <b>Стоимость изготовления паспорта</b> (меняется в зависимостиот срочности)
            </div>
            <?php
            $intervals = iwt_passport_get_intervals();
            foreach ($intervals as $key => $row) {
                $cols = array();

                $row_ = array();
                $row_ = iwt_partition($row, 2);
                $countrows = 0;
                foreach ($row_ as $col => $r1) {
                    $variant_here = str_replace('obrazca', 'obraztsa', $r1[0]['variant']);
                    ?>

                    <!--биометрический старше 18 другие регионы-->
                    <div class="iwt1-price-result <?php echo $variant_here; ?> hidden">
                        <?php foreach ($r1 as $r) { ?>
                            <div class="iwt1-price-form-row iwt1-price-icon-<?php echo $color[$r['color']]; ?> <?php echo $r['color']; ?> clearfix">
                                <div class="iwt1-price-icon1 iwt1-price-dotted">
                                    <div class="iwt1-price-days">
                                        <?php echo $r['days']; ?>
                                        <?php
                                        $l = substr($r['days'], -1);
                                        $suffix = ' дней';
                                        if ($r['days'] <= 10 || $r['days'] > 15) {
                                            if (!strpbrk('-', $r['days'])) {
                                                if (in_array($l, array(2, 3, 4))) {
                                                    $suffix = 'дня';
                                                }
                                                if ($l == 1) {
                                                    $suffix = 'день';
                                                }
                                            }
                                        }
                                        echo $suffix;
                                        ?>
                                    </div>
                                    <div class = "iwt1-price">
                                        <?php echo number_format($r['price'], 0, ',', ' '); ?> р.
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <?php
                }
            }
            ?>
        </div>



        <div class="iwt1-price-small-text">
            <b>Сроки указаны в рабочих днях. Цены актуальны на <?php echo date('d.m.Y'); ?>.</b><br />
            <span class="iwt1-red-text">Обратите внимание, в стоимость уже включена гос. пошлина.
                Если вы ее оплатили — мы вычтем ее из стоимости оформления загранпаспорта. 
            </span>
        </div>



        <div class="iwt1-price-title">
            Необходимые документы:
        </div>

        <?php
        $intervals = iwt_passport_get_intervals(get_the_id());

        foreach ($intervals as $key => $v1) {
            ?>
            <ul class="hidden iwt1-docs-list iwt1-price-docs-list doc_<?php echo str_replace('iwt_variant_', '', str_replace('obrazca', 'obraztsa', $key)); ?>" >
                <?php
                $countrows2 = 0;
                foreach ($v1 as $row) {
                    $countrows2++;
                    if ($countrows2 == 1) {
                        $docs_ = explode(';', $row['documents']);
                        foreach ($docs_ as $doc) {
                            ?><li><?php echo $doc; ?></li>
                            <?php
                        }
                    }
                }
                ?>
            </ul>
        <?php } ?>



        <div class="iwt1-price-title iwt1-price-title-margin">
            Что включено в стоимость:
        </div>
        <?php
        $docs_ = $theme->field('Что включено - через точку с запятой', array('notsingle' => false));
        $docs = explode(';', $docs_);
        ?>
        <ul class="iwt1-docs-list">
            <?php foreach ($docs as $doc) { ?>
                <li><?php echo $doc; ?></li>
            <?php } ?>
        </ul>
    </div>


    <div class="container-fluid">   <?php
        $cartext = $theme->field('Текст рядом с машинкой');
        if (strlen($cartext)) {
            ?>
            <div class="iwt1-dark-icon-block">

                <div class="iwt1-dark-icon-padding">
                    <?php
                    // $cartext = $theme->field('Текст рядом с машинкой');

                    echo $cartext;
                    ?>
                </div>
            </div><?php } ?>

        <ul class="iwt1-form-info">
            <?php
            $arr = array('Обратите внимание', 'Возможные причины отказа', "Преимущества &laquo;Кастур&raquo;");
            foreach ($arr as $k => $v) {
                ?>
                <li class="iwt1-form-info-li <?php if ($k == 0) { ?>active<?php } ?>">
                    <a data-toggle="tab" href="#<?php echo $theme->sanitize($v); ?>" aria-expanded="true"><?php echo $v; ?></a>
                    <span class="iwt1-arrow pull-right"></span>
                </li>
            <?php } ?>

        </ul>


        <div class="tab-content iwt1-tab-content-price">
            <?php foreach ($arr as $k => $v) { ?>
                <div id="<?php echo $theme->sanitize($v); ?>" class="tab-pane fade <?php if ($k == 0) { ?>active<?php } ?> in iwt1-text iwt1-text-16 iwt1-text-light">
                    <p><?php echo $theme->field($v, array('notsingle' => false, 'variant' => 'textarea')); ?></p>
                </div>   
            <?php } ?>
        </div>
    </div><?php get_template_part('nav-oformlenie-grey'); ?>
<?php } ?><script type="text/javascript">
<?php
$passportcontrol = $theme->field('Номера выбранных пунктов в фильтре паспортов через запятую', array('notsingle' => false));
$pc = explode(',', $passportcontrol);
// if(empty($pc)){$pc=array(1,1,1);}
if (!strlen($passportcontrol)) {
    $pc = array(1, 1, 1);
}
?>
    var iwtcp =<?php echo json_encode($pc); ?>;
</script>
<?php
get_footer();
