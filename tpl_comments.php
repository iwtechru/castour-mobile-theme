<?php
require_once( ABSPATH . 'wp-admin/includes/image.php' );
require_once( ABSPATH . 'wp-admin/includes/file.php' );
require_once( ABSPATH . 'wp-admin/includes/media.php' );
?>
<div class="container-fluid">
    <div class="iwt1-pagetitle iwt1-pagetitle-reviews">
        Отзывы клиентов
    </div>
    <div class="row1 iwt1-reviews-top clearfix">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 iwt1-reviews-amount">
            <?php //comments_number('нет отзывов', 'Всего отзывов: <span class="count-com">1</span>', 'Всего отзывов: <b class="count-com">%</b>'); ?>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 iwt1-reviews-btn-wrapper">
            <button type="button" class="iwt1-orange-btn-block iwt1-add-review" id="iwt1-add-review">оставить отзыв</button>
        </div>
    </div>


    <?php
    $args = array(
        "style" => "div",
        "callback" => "altair_comment",
        'reverse_top_level' => true
            //"per_page" => 2 
    );

    (wp_list_comments($args));
    ?>




    <button id="iwt_show_comments" class="btn iwt1-gray-btn iwt1-show-review">показать еще отзывы</button>
</div>


<div class="iwt1-review-form-wrapper">
    <div class="iwt1-review-form-title">Оставить отзыв</div>
    <?php
    $fields = array(
        'author' => '<div class="comment-form-author">' . ( $req ? '' : '' ) .
        '<input id="author" name="author" class="iwt1-review-input" placeholder="Ваше имя" type="text" value="" /></div>',
            // 'comment_field' => '<div class="comment-form-comment"><textarea placeholder="Отзыв" id="comment" name="comment" aria-describedby="form-allowed-tags" aria-required="true"></textarea></div>'
    );


    $comments_args = array(
        'fields' => apply_filters('comment_form_default_fields', $fields),
        'comment_notes_before' => '',
        'comment_notes_after' => '',
        'comment_field' => '<div class="comment-form-comment "><textarea class="iwt1-review-textarea" placeholder="Отзыв" id="comment" name="comment" aria-describedby="form-allowed-tags" aria-required="true" required></textarea></div>',
        'label_submit' => 'Добавить отзыв',
        'title_reply' => 'Оставить отзыв'
    );

    comment_form($comments_args);
    ?>             
</div>