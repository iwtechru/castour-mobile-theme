<?php
get_header();
global $theme;
while (have_posts()) {
    the_post();
    ?>
    <div class="container-fluid">
        <div class="iwt1-pagetitle iwt1-pagetitle-company">
            <?php the_title(); ?>
        </div>
        <?php get_template_part('block-extratext'); ?>
        <div class="iwt1-sertificate-wrapper clearfix">
            <div class="iwt1-margin-bottom-20">
                <a href="<?php echo $theme->get_thumb_src(get_the_ID());?>">
                    <img src="<?php echo $theme->get_thumb_src(get_the_ID()); ?>" alt="" class="iwt1-sertificate-img img-responsive"/>
                </a>
                <?php echo $theme->field('Текст справа от сертификата', array('notsingle' => false, 'variant' => 'textarea')); ?>
            </div>            
        </div>
        <?php
        $pid = get_the_ID();
        $content = explode('<hr />', do_shortcode(get_the_content()));
        $content[0] = str_replace('<h2', '<!--!></!--!><h2', $content[0]);
        $content0 = explode('<!--!></!--!>', $content[0]);
        $fat = 'Преимущества нашей компании перед конкурентами';
        $files = $theme->attachments($fat);
        //print_r($content0);
        ?>
        <div class="iwt1-text iwt1-text-16 iwt1-text-light">
            <?php echo $content0[1]; ?>
        </div>

        <div class="iwtcontent2">
            <?php echo $content0[2]; ?>
        </div>

        <div class="row iwt1-advantage-block">
            <div class="iwt1-sertificate-title iwt1-advantage-title text-center">
                <?php echo $fat; ?>
            </div>
            <?php
            $countblocks = 0;
            for ($i = 0; $i < 1; $i++) {
                foreach ($files as $key => $file) {
                    $countblocks++;
                    ?>
                    <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12 iwt1-advantage-item">
                        <div class="iwt1-advantage-icon iwt1-advantage-icon-<?php echo $countblocks; ?>" ></div>
                        <div class="iwt1-advantage-text iwt1-advantage-text-<?php echo $countblocks; ?>"><?php echo $file['fields']['title']; ?></div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="iwtcontent3">
            <?php echo $content[1]; ?>
        </div>


        <div class="iwt1-company-info-wrapper">
            <div class="iwt1-company-info-icon"></div>
            <div class="iwt1-company-info-text">
                <?php echo $theme->field('Текст справа от восклицательного знака', array('pid' => $pid, 'notsingle' => false, 'variant' => 'textarea')); ?>
            </div>
        </div>

        <div class="iwtcontent4">
            <?php echo $content[2]; ?>
        </div>
    </div>
    <?php
}
get_footer();
