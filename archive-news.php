<?php
get_header();
global $theme;
?><div class="container-fluid">
    <div class="iwt1-pagetitle iwt1-pagetitle-news">
        <?php echo $theme->sitevar('Заголовок страницы ' . get_post_type(), array('default' => 'Полезные новости')); ?>
    </div>
    <?php get_template_part('block', 'extratext'); ?>
    <div class="iwt1-news-col-wrapper">
        <?php
        while (have_posts()) {
            the_post();
            ?>
            <div class="iwt1-news-col-mobile">
                <?php
                $img = $theme->get_thumb_src();
                if (strlen($img)) {
                    ?>
                    <a href="<?php echo post_permalink(); ?>" class="iwt1-news-img-link">
                        <img src="<?php echo $img; ?>" alt="<?php the_title(); ?>" class="img-responsive" />
                    </a>
                <?php } ?>
                <a href="<?php echo post_permalink(); ?>" class="iwt1-news-link"><?php the_title();?></a>
                <?php if (get_post_type() == 'news') { ?><div class="iwt1-ads-date iwt1-ads-date-mobile"><?php echo $theme->get_the_time('%d %FS %Y'); ?></div><?php } ?>
                <div class="iwt1-text iwt1-text-16 iwt1-text-light">
                    <p>
                        <?php the_excerpt(); ?>
                    </p>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php
get_footer();
