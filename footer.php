<?php global $theme; ?>
<div class="iwt1-footer text-center">
    <?php
    for ($i = 1; $i <= 2; $i++) {
        $phone_ = $theme->sitevar('Телефон №' . $i . ' в шапке', array('default' => "+7 (999)999-99-99"));
        $phone_content = explode(')', $phone_);
        $phone_link = preg_replace("/[^0-9]/", "", $phone_);
//	$phone_link=preg_replace('/\A8/','7',$phone_link);
        ?>                        

        <a href="tel:<?php echo $phone_link; ?>" class="iwt1-footer-mobile-phone"><b><?php echo $phone_; ?></b></a><?php if ($i < 2) { ?><br/><?php } ?>
    <?php } ?>

    <div class="iwt1-social-wrapper clearfix">
        <span class="pull-left">Присоединяйтесь: </span>
        <a href="http://vk.com/castour_ru" target="_blank" class="iwt1-social-link iwt1-vk"></a>
        <a href="https://www.facebook.com/castour.ru" target="_blank" class="iwt1-social-link iwt1-fb"></a>
    </div>

    <div class="iwt1-dark-footer">
        © www.castour.ru, 2009-2015 Кастур. Помощь в оформлении загранпаспорта. Все права защищены. <br/>
        Наш адрес: <?php echo $theme->sitevar('Адрес в подвале', array('default' => 'г. Москва, ул. Трубная, д.32, стр.4, оф.3.')); ?>
    </div>
    <a href="<?php
    echo add_query_arg(array('mobile_switch' => 'desktop'), $_SERVER['REQUEST_URI']);
    ?>" class="iwt1-fullversion-link">Полная версия сайта</a>
</div>



<!--окно обратного звонка-->
<div class="modal fade" id="callme" data-backdrop="static">
    <div class="modal-dialog iwt1-contact-modal-dialog">
        <div class="iwt1-modal-header">
            <button type="button" class="close iwt1-close" data-dismiss="modal" aria-label="Close"></button>
            Заказать звонок
        </div>
        <div class="text-center iwt1-contact-text">Оставьте заявку и мы с вами свяжемся в ближайшее время.</div>
        <?php
        //echo (do_shortcode('[rhcf_form]block-callback.php[/rhcf_form]')); 
        if (function_exists('ninja_forms_display_form')) {
            ninja_forms_display_form(5);
        }
        ?>
    </div>
</div>
<!--окно обратного звонка-->
<?php if (!is_user_logged_in()) { ?>
    <!--окно авторизации-->
    <form class="modal fade" id="auth" action="login" method="post">
        <div class="modal-dialog iwt1-auth-modal-dialog">
            <div class="iwt1-modal-header">
                <button type="button" class="close iwt1-close" data-dismiss="modal" aria-label="Close"></button>
                Авторизация
            </div>
            <p class="status"></p>
            <div class="iwt1-input-wrapper">
                <input id="username" name="username" type="text" class="iwt1-input-mobile" placeholder="Логин" />
            </div>
            <div class="iwt1-input-wrapper">
                <input id="password" type="password" name="password" class="iwt1-input-mobile" placeholder="Пароль" />
            </div>
            <input type="submit" class="btn iwt1-orange-btn iwt1-orange-btn-block iwt1-login-btn" value="Войти">
            <?php wp_nonce_field('ajax-login-nonce', 'security'); ?>
        </div>
    </form>
    <!--окно авторизации-->

    <!--окно восстановления пароля-->
    <div class="modal fade" id="recovery">
        <div class="modal-dialog iwt1-recovery-modal-dialog">
            <div class="iwt1-modal-header">
                <button type="button" class="close iwt1-close" data-dismiss="modal" aria-label="Close"></button>
                Восстановление пароля
            </div>
            <div class="text-left iwt1-contact-text">
                Введите адрес электронной почты, и мы отправим вам ссылку на сброс пароля.
            </div>
            <div class="iwt1-input-wrapper">
                <input type="text" class="iwt1-input-mobile" placeholder="Электронная почта" />
            </div>
            <button type="button" class="btn iwt1-orange-btn iwt1-orange-btn-block iwt1-contact-btn">Отправить</button>
        </div>
    </div>
    <!--окно восстановления пароля-->
<?php } ?>
<script type='text/javascript' src='//ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.3.min.js?ver=4.1'></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-placeholder/2.0.8/jquery.placeholder.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<?php if (2 == 1) { ?>
    <script type='text/javascript' src='js/ui.core.min.js'></script>
    <script type='text/javascript' src='js/datepicker.min.js'></script>
    <script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/responsive.js"></script>
<?php } ?>
<?php wp_footer(); ?>
<?php echo $theme->sitevar('Подвал - счетчики для мобильной и полной версий');
 for($i=1;$i<=4;$i++){ echo $theme->sitevar('Подвал - счетчики для мобильной и полной версий №'.$i);}?>
</body>
</html>
